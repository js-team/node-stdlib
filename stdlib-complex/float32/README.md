<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Complex64

> 64-bit complex number.

<!-- Section to include introductory text. Make sure to keep an empty line after the intro `section` element and another before the `/section` close. -->

<section class="intro">

</section>

<!-- /.intro -->

<!-- Package usage documentation. -->

<section class="usage">

## Usage

```javascript
var Complex64 = require( '@stdlib/complex/float32' );
```

#### Complex64( real, imag )

64-bit complex number constructor, where `real` and `imag` are the **real** and **imaginary** components, respectively.

```javascript
var z = new Complex64( 5.0, 3.0 );
// returns <Complex64>
```

* * *

## Properties

#### Complex64.BYTES_PER_ELEMENT

Size (in bytes) of each component.

```javascript
var nbytes = Complex64.BYTES_PER_ELEMENT;
// returns 4
```

#### Complex64.prototype.BYTES_PER_ELEMENT

Size (in bytes) of each component.

```javascript
var z = new Complex64( 5.0, 3.0 );

var nbytes = z.BYTES_PER_ELEMENT;
// returns 4
```

#### Complex64.prototype.byteLength

Length (in bytes) of a complex number.

```javascript
var z = new Complex64( 5.0, 3.0 );

var nbytes = z.byteLength;
// returns 8
```

### Instance

A `Complex64` instance has the following properties...

#### re

A **read-only** property returning the **real** component.

```javascript
var z = new Complex64( 5.0, 3.0 );

var re = z.re;
// returns 5.0
```

#### im

A **read-only** property returning the **imaginary** component.

```javascript
var z = new Complex64( 5.0, -3.0 );

var im = z.im;
// returns -3.0
```

* * *

## Methods

### Accessor Methods

These methods do **not** mutate a `Complex64` instance and, instead, return a complex number representation.

#### Complex64.prototype.toString()

Returns a `string` representation of a `Complex64` instance.

```javascript
var z = new Complex64( 5.0, 3.0 );
var str = z.toString();
// returns '5 + 3i'

z = new Complex64( -5.0, -3.0 );
str = z.toString();
// returns '-5 - 3i'
```

#### Complex64.prototype.toJSON()

Returns a [JSON][json] representation of a `Complex64` instance. [`JSON.stringify()`][mdn-json-stringify] implicitly calls this method when stringifying a `Complex64` instance.

```javascript
var z = new Complex64( 5.0, -3.0 );

var o = z.toJSON();
/*
  {
    "type": "Complex64",
    "re": 5.0,
    "im": -3.0
  }
*/
```

To [revive][mdn-json-parse] a `Complex64` number from a [JSON][json] `string`, see [@stdlib/complex/reviver-float32][@stdlib/complex/reviver-float32].

</section>

<!-- /.usage -->

* * *

<!-- Package usage notes. Make sure to keep an empty line after the `section` element and another before the `/section` close. -->

<section class="notes">

## Notes

-   Both the **real** and **imaginary** components are stored as single-precision floating-point numbers.

</section>

<!-- /.notes -->

* * *

<!-- Package usage examples. -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var Complex64 = require( '@stdlib/complex/float32' );

var z = new Complex64( 3.0, -2.0 );

console.log( 'type: %s', typeof z );
// => type: object

console.log( 'str: %s', z );
// => str: 3 - 2i

console.log( 'real: %d', z.re );
// => real: 3.0

console.log( 'imag: %d', z.im );
// => imag: -2.0

console.log( 'JSON: %s', JSON.stringify( z ) );
// => JSON: {"type":"Complex64","re":3,"im":-2}
```

</section>

<!-- /.examples -->

<!-- Section to include cited references. If references are included, add a horizontal rule *before* the section. Make sure to keep an empty line after the `section` element and another before the `/section` close. -->

<section class="references">

</section>

<!-- /.references -->

<!-- Section for all links. Make sure to keep an empty line after the `section` element and another before the `/section` close. -->

<section class="links">

[json]: http://www.json.org/

[mdn-json-stringify]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify

[mdn-json-parse]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse

[@stdlib/complex/reviver-float32]: https://github.com/stdlib-js/complex/tree/main/reviver-float32

</section>

<!-- /.links -->

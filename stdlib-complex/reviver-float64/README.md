<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Reviver

> Revive a JSON-serialized 128-bit [complex number][@stdlib/complex/float64].

<!-- Section to include introductory text. Make sure to keep an empty line after the intro `section` element and another before the `/section` close. -->

<section class="intro">

</section>

<!-- /.intro -->

<!-- Package usage documentation. -->

<section class="usage">

## Usage

```javascript
var reviver = require( '@stdlib/complex/reviver-float64' );
```

#### reviver( key, value )

Revives a JSON-serialized 128-bit [complex number][@stdlib/complex/float64].

```javascript
var parseJSON = require( '@stdlib/utils/parse-json' );

var str = '{"type":"Complex128","re":5,"im":3}';

var z = parseJSON( str, reviver );
// returns <Complex128>
```

For details on the JSON serialization format, see [`Complex128`][@stdlib/complex/float64].

</section>

<!-- /.usage -->

<!-- Package usage notes. Make sure to keep an empty line after the `section` element and another before the `/section` close. -->

<section class="notes">

</section>

<!-- /.notes -->

<!-- Package usage examples. -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var Complex128 = require( '@stdlib/complex/float64' );
var parseJSON = require( '@stdlib/utils/parse-json' );
var reviver = require( '@stdlib/complex/reviver-float64' );

var z = new Complex128( 5.0, 3.0 );
var str = JSON.stringify( z );
// returns '{"type":"Complex128","re":5,"im":3}'

var w = parseJSON( str, reviver );
if ( w instanceof Error ) {
    throw w;
}
var bool = ( w instanceof z.constructor );
// returns true

bool = ( w.re === z.re );
// returns true

bool = ( w.im === z.im );
// returns true
```

</section>

<!-- /.examples -->

<!-- Section to include cited references. If references are included, add a horizontal rule *before* the section. Make sure to keep an empty line after the `section` element and another before the `/section` close. -->

<section class="references">

</section>

<!-- /.references -->

<!-- Section for all links. Make sure to keep an empty line after the `section` element and another before the `/section` close. -->

<section class="links">

[@stdlib/complex/float64]: https://github.com/stdlib-js/complex/tree/main/float64

</section>

<!-- /.links -->

<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Linspace

> Generate a linearly spaced numeric array.

<section class="usage">

## Usage

```javascript
var linspace = require( '@stdlib/array/linspace' );
```

#### linspace( start, stop\[, length] )

Generates a linearly spaced numeric `array`. If a `length` is not provided, the default output `array` length is `100`.

```javascript
var arr = linspace( 0, 100, 6 );
// returns [ 0, 20, 40, 60, 80, 100 ]
```

</section>

<!-- /.usage -->

<section class="notes">

## Notes

-   The output `array` is guaranteed to include the `start` and `stop` values. Beware, however, that values between the `start` and `stop` are subject to floating-point errors. Hence,

    ```javascript
    var arr = linspace( 0, 1, 3 );
    // returns [ 0, ~0.5, 1 ]
    ```

    where `arr[1]` is only guaranteed to be approximately equal to `0.5`. If you desire more control over element precision, consider using [roundn][@stdlib/math/base/special/roundn]:

    ```javascript
    var roundn = require( '@stdlib/math/base/special/roundn' );

    // Create an array subject to floating-point errors:
    var arr = linspace( 0, 1, 21 );

    // Round each value to the nearest hundredth:
    var out = [];
    var i;
    for ( i = 0; i < arr.length; i++ ) {
        out.push( roundn( arr[ i ], -2 ) );
    }

    console.log( out.join( '\n' ) );
    ```

</section>

<!-- /.notes -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var linspace = require( '@stdlib/array/linspace' );
var out;

// Default behavior:
out = linspace( 0, 10 );
console.log( out.join( '\n' ) );

// Specify length:
out = linspace( 0, 10, 10 );
console.log( out.join( '\n' ) );

out = linspace( 0, 10, 11 );
console.log( out.join( '\n' ) );

// Create an array with decremented values:
out = linspace( 10, 0, 11 );
console.log( out.join( '\n' ) );
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/math/base/special/roundn]: https://github.com/stdlib-js/math-base-special-roundn

</section>

<!-- /.links -->

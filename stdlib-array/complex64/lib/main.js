/* eslint-disable no-restricted-syntax, max-lines */

/**
* @license Apache-2.0
*
* Copyright (c) 2018 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

'use strict';

// MODULES //

var isNonNegativeInteger = require( '@stdlib/assert/is-nonnegative-integer' ).isPrimitive;
var isArrayLikeObject = require( '@stdlib/assert/is-array-like-object' );
var isCollection = require( '@stdlib/assert/is-collection' );
var isArrayBuffer = require( '@stdlib/assert/is-arraybuffer' );
var isObject = require( '@stdlib/assert/is-object' );
var isArray = require( '@stdlib/assert/is-array' );
var isFunction = require( '@stdlib/assert/is-function' );
var isComplexLike = require( '@stdlib/assert/is-complex-like' );
var isEven = require( '@stdlib/math/base/assert/is-even' );
var isInteger = require( '@stdlib/math/base/assert/is-integer' );
var hasIteratorSymbolSupport = require( '@stdlib/assert/has-iterator-symbol-support' );
var ITERATOR_SYMBOL = require( '@stdlib/symbol/iterator' );
var defineProperty = require( '@stdlib/utils/define-property' );
var Float32Array = require( './../../float32' );
var Complex64 = require( '@stdlib/complex/float32' );
var real = require( '@stdlib/complex/real' );
var imag = require( '@stdlib/complex/imag' );
var fromIterator = require( './from_iterator.js' );
var fromIteratorMap = require( './from_iterator_map.js' );
var fromArray = require( './from_array.js' );


// VARIABLES //

var BYTES_PER_ELEMENT = Float32Array.BYTES_PER_ELEMENT * 2;
var HAS_ITERATOR_SYMBOL = hasIteratorSymbolSupport();


// FUNCTIONS //

/**
* Returns a boolean indicating if a value is a complex typed array.
*
* @private
* @param {*} value - value to test
* @returns {boolean} boolean indicating if a value is a complex typed array
*/
function isComplexArray( value ) {
	return (
		value instanceof Complex64Array ||
		(
			typeof value === 'object' &&
			value !== null &&
			(
				value.constructor.name === 'Complex64Array' ||
				value.constructor.name === 'Complex128Array'
			) &&
			typeof value._length === 'number' && // eslint-disable-line no-underscore-dangle

			// NOTE: we don't perform a more rigorous test here for a typed array for performance reasons, as robustly checking for a typed array instance could require walking the prototype tree and performing relatively expensive constructor checks...
			typeof value._buffer === 'object' // eslint-disable-line no-underscore-dangle
		)
	);
}

/**
* Returns a boolean indicating if a value is a complex typed array constructor.
*
* @private
* @param {*} value - value to test
* @returns {boolean} boolean indicating if a value is a complex typed array constructor
*/
function isComplexArrayConstructor( value ) {
	return (
		value === Complex64Array ||

		// NOTE: weaker test in order to avoid a circular dependency with Complex128Array...
		value.name === 'Complex128Array'
	);
}


// MAIN //

/**
* 64-bit complex number array constructor.
*
* @constructor
* @param {(NonNegativeInteger|TypedArray|ArrayLikeObject|ArrayBuffer|Iterable)} [arg] - length, typed array, array-like object, or buffer
* @param {NonNegativeInteger} [byteOffset=0] - byte offset
* @param {NonNegativeInteger} [length] - view length
* @throws {RangeError} ArrayBuffer byte length must be a multiple of `8`
* @throws {RangeError} array-like object and typed array input arguments must have a length which is a multiple of two
* @throws {TypeError} if provided only a single argument, must provide a valid argument
* @throws {TypeError} byte offset must be a nonnegative integer
* @throws {RangeError} byte offset must be a multiple of `8`
* @throws {TypeError} view length must be a positive multiple of `8`
* @throws {RangeError} must provide sufficient memory to accommodate byte offset and view length requirements
* @throws {TypeError} an iterator must return either a two element array containing real and imaginary components or a complex number
* @returns {Complex64Array} complex number array
*
* @example
* var arr = new Complex64Array();
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 0
*
* @example
* var arr = new Complex64Array( 2 );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 2
*
* @example
* var arr = new Complex64Array( [ 1.0, -1.0 ] );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 1
*
* @example
* var ArrayBuffer = require( '@stdlib/array/buffer' );
*
* var buf = new ArrayBuffer( 16 );
* var arr = new Complex64Array( buf );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 2
*
* @example
* var ArrayBuffer = require( '@stdlib/array/buffer' );
*
* var buf = new ArrayBuffer( 16 );
* var arr = new Complex64Array( buf, 8 );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 1
*
* @example
* var ArrayBuffer = require( '@stdlib/array/buffer' );
*
* var buf = new ArrayBuffer( 32 );
* var arr = new Complex64Array( buf, 8, 2 );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 2
*/
function Complex64Array() {
	var byteOffset;
	var nargs;
	var buf;
	var len;

	nargs = arguments.length;
	if ( !(this instanceof Complex64Array) ) {
		if ( nargs === 0 ) {
			return new Complex64Array();
		}
		if ( nargs === 1 ) {
			return new Complex64Array( arguments[0] );
		}
		if ( nargs === 2 ) {
			return new Complex64Array( arguments[0], arguments[1] );
		}
		return new Complex64Array( arguments[0], arguments[1], arguments[2] );
	}
	// Create the underlying data buffer...
	if ( nargs === 0 ) {
		buf = new Float32Array( 0 ); // backward-compatibility
	} else if ( nargs === 1 ) {
		if ( isNonNegativeInteger( arguments[0] ) ) {
			buf = new Float32Array( arguments[0]*2 );
		} else if ( isCollection( arguments[0] ) ) {
			buf = arguments[ 0 ];
			len = buf.length;

			// If provided a "generic" array, peak at the first value, and, if the value is a complex number, try to process as an array of complex numbers, falling back to "normal" typed array initialization if we fail and ensuring consistency if the first value had not been a complex number...
			if ( len && isArray( buf ) && isComplexLike( buf[0] ) ) {
				buf = fromArray( new Float32Array( len*2 ), buf );
				if ( buf === null ) {
					// We failed and we are now forced to allocate a new array :-(
					if ( !isEven( len ) ) {
						throw new RangeError( 'invalid argument. Array-like object input arguments must have a length which is a multiple of two. Length: `'+len+'`.' );
					}
					// We failed, so fall back to directly setting values...
					buf = new Float32Array( arguments[0] );
				}
			} else {
				if ( !isEven( len ) ) {
					throw new RangeError( 'invalid argument. Array-like object and typed array input arguments must have a length which is a multiple of two. Length: `'+len+'`.' );
				}
				buf = new Float32Array( buf );
			}
		} else if ( isArrayBuffer( arguments[0] ) ) {
			buf = arguments[ 0 ];
			if ( !isInteger( buf.byteLength/BYTES_PER_ELEMENT ) ) {
				throw new RangeError( 'invalid argument. ArrayBuffer byte length must be a multiple of '+BYTES_PER_ELEMENT+'. Byte length: `'+buf.byteLength+'`.' );
			}
			buf = new Float32Array( buf );
		} else if ( isObject( arguments[0] ) ) {
			buf = arguments[ 0 ];
			if ( HAS_ITERATOR_SYMBOL === false ) {
				throw new TypeError( 'invalid argument. Environment lacks Symbol.iterator support. Must provide a length, ArrayBuffer, typed array, or array-like object. Value: `'+buf+'`.' );
			}
			if ( !isFunction( buf[ ITERATOR_SYMBOL ] ) ) {
				throw new TypeError( 'invalid argument. Must provide a length, ArrayBuffer, typed array, array-like object, or an iterable. Value: `'+buf+'`.' );
			}
			buf = buf[ ITERATOR_SYMBOL ]();
			if ( !isFunction( buf.next ) ) {
				throw new TypeError( 'invalid argument. Must provide a length, ArrayBuffer, typed array, array-like object, or an iterable.' );
			}
			buf = fromIterator( buf );
			if ( buf instanceof Error ) {
				throw buf;
			}
			buf = new Float32Array( buf );
		} else {
			throw new TypeError( 'invalid argument. Must provide a length, ArrayBuffer, typed array, array-like object, or an iterable. Value: `'+arguments[0]+'`.' );
		}
	} else {
		buf = arguments[ 0 ];
		if ( !isArrayBuffer( buf ) ) {
			throw new TypeError( 'invalid argument. First argument must be an array buffer. Value: `'+buf+'`.' );
		}
		byteOffset = arguments[ 1 ];
		if ( !isNonNegativeInteger( byteOffset ) ) {
			throw new TypeError( 'invalid argument. Byte offset must be a nonnegative integer. Value: `'+byteOffset+'`.' );
		}
		if ( !isInteger( byteOffset/BYTES_PER_ELEMENT ) ) {
			throw new RangeError( 'invalid argument. Byte offset must be a multiple of '+BYTES_PER_ELEMENT+'. Value: `'+byteOffset+'`.' );
		}
		if ( nargs === 2 ) {
			len = buf.byteLength - byteOffset;
			if ( !isInteger( len/BYTES_PER_ELEMENT ) ) {
				throw new RangeError( 'invalid arguments. ArrayBuffer view byte length must be a multiple of '+BYTES_PER_ELEMENT+'. View byte length: `'+len+'`.' );
			}
			buf = new Float32Array( buf, byteOffset );
		} else {
			len = arguments[ 2 ];
			if ( !isNonNegativeInteger( len ) ) {
				throw new TypeError( 'invalid argument. Length must be a nonnegative integer. Value: `'+len+'`.' );
			}
			if ( (len*BYTES_PER_ELEMENT) > (buf.byteLength-byteOffset) ) {
				throw new RangeError( 'invalid arguments. ArrayBuffer has insufficient capacity. Either decrease the array length or provide a bigger buffer. Minimum capacity: `'+(len*BYTES_PER_ELEMENT)+'`.' );
			}
			buf = new Float32Array( buf, byteOffset, len*2 );
		}
	}
	defineProperty( this, '_buffer', {
		'configurable': false,
		'enumerable': false,
		'writable': false,
		'value': buf
	});
	defineProperty( this, '_length', {
		'configurable': false,
		'enumerable': false,
		'writable': false,
		'value': buf.length / 2
	});

	return this;
}

/**
* Size (in bytes) of each array element.
*
* @name BYTES_PER_ELEMENT
* @memberof Complex64Array
* @type {PositiveInteger}
* @default 8
*
* @example
* var nbytes = Complex64Array.BYTES_PER_ELEMENT;
* // returns 8
*/
defineProperty( Complex64Array, 'BYTES_PER_ELEMENT', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': BYTES_PER_ELEMENT
});

/**
* Constructor name.
*
* @name name
* @memberof Complex64Array
* @type {string}
* @default 'Complex64Array'
*
* @example
* var str = Complex64Array.name;
* // returns 'Complex64Array'
*/
defineProperty( Complex64Array, 'name', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': 'Complex64Array'
});

/**
* Creates a new 64-bit complex number array from an array-like object or an iterable.
*
* @name from
* @memberof Complex64Array
* @type {Function}
* @param {(ArrayLikeObject|Iterable)} src - array-like object or iterable
* @param {Function} [clbk] - callback to invoke for each source element
* @param {*} [thisArg] - context
* @throws {TypeError} `this` context must be a constructor
* @throws {TypeError} `this` must be a complex number array
* @throws {TypeError} first argument must be an array-like object or an iterable
* @throws {TypeError} second argument must be a function
* @throws {RangeError} array-like objects must have a length which is a multiple of two
* @throws {TypeError} an iterator must return either a two element array containing real and imaginary components or a complex number
* @throws {TypeError} when provided an iterator, a callback must return either a two element array containing real and imaginary components or a complex number
* @returns {Complex64Array} 64-bit complex number array
*
* @example
* var arr = Complex64Array.from( [ 1.0, -1.0 ] );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 1
*
* @example
* var Complex64 = require( '@stdlib/complex/float32' );
*
* var arr = Complex64Array.from( [ new Complex64( 1.0, 1.0 ) ] );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 1
*
* @example
* var Complex64 = require( '@stdlib/complex/float32' );
* var real = require( '@stdlib/complex/real' );
* var imag = require( '@stdlib/complex/imag' );
*
* function clbk( v ) {
*     return new Complex64( real(v)*2.0, imag(v)*2.0 );
* }
*
* var arr = Complex64Array.from( [ new Complex64( 1.0, 1.0 ) ], clbk );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 1
*/
defineProperty( Complex64Array, 'from', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': function from( src ) {
		var thisArg;
		var nargs;
		var clbk;
		var out;
		var buf;
		var tmp;
		var len;
		var flg;
		var v;
		var i;
		var j;
		if ( !isFunction( this ) ) {
			throw new TypeError( 'invalid invocation. `this` context must be a constructor.' );
		}
		if ( !isComplexArrayConstructor( this ) ) {
			throw new TypeError( 'invalid invocation. `this` is not a complex number array.' );
		}
		nargs = arguments.length;
		if ( nargs > 1 ) {
			clbk = arguments[ 1 ];
			if ( !isFunction( clbk ) ) {
				throw new TypeError( 'invalid argument. Second argument must be a function. Value: `'+clbk+'`.' );
			}
			if ( nargs > 2 ) {
				thisArg = arguments[ 2 ];
			}
		}
		if ( isCollection( src ) ) {
			if ( clbk ) {
				// Note: array contents affect how we iterate over a provided data source. If only complex numbers, we can extract real and imaginary components. Otherwise, we assume a strided array where real and imaginary components are interleaved. In the former case, we expect a callback to return real and imaginary components (possibly as a complex number). In the latter case, we expect a callback to return *either* a real or imaginary component.

				// Detect whether we've been provided an array of complex numbers...
				len = src.length;
				for ( i = 0; i < len; i++ ) {
					if ( !isComplexLike( src[ i ] ) ) {
						flg = true;
						break;
					}
				}
				// If an array does not contain only complex numbers, then we assume interleaved real and imaginary components...
				if ( flg ) {
					if ( !isEven( len ) ) {
						throw new RangeError( 'invalid argument. First argument must have a length which is a multiple of two. Length: `'+len+'`.' );
					}
					out = new this( len/2 );
					buf = out._buffer; // eslint-disable-line no-underscore-dangle
					for ( i = 0; i < len; i++ ) {
						buf[ i ] = clbk.call( thisArg, src[ i ], i );
					}
				}
				// If an array contains only complex numbers, then we need to extract real and imaginary components...
				else {
					out = new this( len );
					buf = out._buffer; // eslint-disable-line no-underscore-dangle
					j = 0;
					for ( i = 0; i < len; i++ ) {
						v = clbk.call( thisArg, src[ i ], i );
						if ( isComplexLike( v ) ) {
							buf[ j ] = real( v );
							buf[ j+1 ] = imag( v );
						} else if ( isArrayLikeObject( v ) && v.length >= 2 ) {
							buf[ j ] = v[ 0 ];
							buf[ j+1 ] = v[ 1 ];
						} else {
							throw new TypeError( 'invalid argument. Callback must return either a two-element array containing real and imaginary components or a complex number. Value: `'+v+'`.' );
						}
						j += 2; // stride
					}
				}
			} else {
				out = new this( src );
			}
		} else if ( isObject( src ) && HAS_ITERATOR_SYMBOL && isFunction( src[ ITERATOR_SYMBOL ] ) ) { // eslint-disable-line max-len
			buf = src[ ITERATOR_SYMBOL ]();
			if ( !isFunction( buf.next ) ) {
				throw new TypeError( 'invalid argument. First argument must be an array-like object or an iterable.' );
			}
			if ( clbk ) {
				tmp = fromIteratorMap( buf, clbk, thisArg );
			} else {
				tmp = fromIterator( buf );
			}
			if ( tmp instanceof Error ) {
				throw tmp;
			}
			len = tmp.length / 2;
			out = new this( len );
			buf = out._buffer; // eslint-disable-line no-underscore-dangle
			for ( i = 0; i < len; i++ ) {
				buf[ i ] = tmp[ i ];
			}
		} else {
			throw new TypeError( 'invalid argument. First argument must be an array-like object or an iterable. Value: `'+src+'`.' );
		}
		return out;
	}
});

/**
* Creates a new 64-bit complex number array from a variable number of arguments.
*
* @name of
* @memberof Complex64Array
* @type {Function}
* @param {...*} element - array elements
* @throws {TypeError} `this` context must be a constructor
* @throws {TypeError} `this` must be a complex number array
* @returns {Complex64Array} 64-bit complex number array
*
* @example
* var arr = Complex64Array.of( 1.0, 1.0, 1.0, 1.0 );
* // returns <Complex64Array>
*
* var len = arr.length;
* // returns 2
*/
defineProperty( Complex64Array, 'of', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': function of() {
		var args;
		var i;
		if ( !isFunction( this ) ) {
			throw new TypeError( 'invalid invocation. `this` context must be a constructor.' );
		}
		if ( !isComplexArrayConstructor( this ) ) {
			throw new TypeError( 'invalid invocation. `this` is not a complex number array.' );
		}
		args = [];
		for ( i = 0; i < arguments.length; i++ ) {
			args.push( arguments[ i ] );
		}
		return new this( args );
	}
});

/**
* Pointer to the underlying data buffer.
*
* @name buffer
* @memberof Complex64Array.prototype
* @type {ArrayBuffer}
*
* @example
* var arr = new Complex64Array( 10 );
*
* var buf = arr.buffer;
* // returns <ArrayBuffer>
*/
defineProperty( Complex64Array.prototype, 'buffer', {
	'configurable': false,
	'enumerable': false,
	'get': function get() {
		return this._buffer.buffer;
	}
});

/**
* Size (in bytes) of the array.
*
* @name byteLength
* @memberof Complex64Array.prototype
* @type {NonNegativeInteger}
*
* @example
* var arr = new Complex64Array( 10 );
*
* var byteLength = arr.byteLength;
* // returns 80
*/
defineProperty( Complex64Array.prototype, 'byteLength', {
	'configurable': false,
	'enumerable': false,
	'get': function get() {
		return this._buffer.byteLength;
	}
});

/**
* Offset (in bytes) of the array from the start of its underlying `ArrayBuffer`.
*
* @name byteOffset
* @memberof Complex64Array.prototype
* @type {NonNegativeInteger}
*
* @example
* var arr = new Complex64Array( 10 );
*
* var byteOffset = arr.byteOffset;
* // returns 0
*/
defineProperty( Complex64Array.prototype, 'byteOffset', {
	'configurable': false,
	'enumerable': false,
	'get': function get() {
		return this._buffer.byteOffset;
	}
});

/**
* Size (in bytes) of each array element.
*
* @name BYTES_PER_ELEMENT
* @memberof Complex64Array.prototype
* @type {PositiveInteger}
* @default 8
*
* @example
* var arr = new Complex64Array( 10 );
*
* var nbytes = arr.BYTES_PER_ELEMENT;
* // returns 8
*/
defineProperty( Complex64Array.prototype, 'BYTES_PER_ELEMENT', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': Complex64Array.BYTES_PER_ELEMENT
});

/**
* Copies a sequence of elements within the array to the position starting at `target`.
*
* @name copyWithin
* @memberof Complex64Array.prototype
* @type {Function}
* @param {integer} target - index at which to start copying elements
* @param {integer} start - source index at which to copy elements from
* @param {integer} [end] - source index at which to stop copying elements from
* @throws {TypeError} `this` must be a complex number array
* @returns {Complex64Array} modified array
*
* @example
* var Complex64 = require( '@stdlib/complex/float32' );
* var real = require( '@stdlib/complex/real' );
* var imag = require( '@stdlib/complex/imag' );
*
* var arr = new Complex64Array( 4 );
*
* // Set the array elements:
* arr.set( new Complex64( 1.0, 1.0 ), 0 );
* arr.set( new Complex64( 2.0, 2.0 ), 1 );
* arr.set( new Complex64( 3.0, 3.0 ), 2 );
* arr.set( new Complex64( 4.0, 4.0 ), 3 );
*
* // Copy the first two elements to the last two elements:
* arr.copyWithin( 2, 0, 2 );
*
* // Get the last array element:
* var z = arr.get( 3 );
*
* var re = real( z );
* // returns 2.0
*
* var im = imag( z );
* // returns 2.0
*/
defineProperty( Complex64Array.prototype, 'copyWithin', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': function copyWithin( target, start ) {
		if ( !isComplexArray( this ) ) {
			throw new TypeError( 'invalid invocation. `this` is not a complex number array.' );
		}
		// FIXME: prefer a functional `copyWithin` implementation which addresses lack of universal browser support (e.g., IE11 and Safari) or ensure that typed arrays are polyfilled
		if ( arguments.length === 2 ) {
			this._buffer.copyWithin( target*2, start*2 );
		} else {
			this._buffer.copyWithin( target*2, start*2, arguments[2]*2 );
		}
		return this;
	}
});

/**
* Returns an iterator for iterating over array key-value pairs.
*
* @name entries
* @memberof Complex64Array.prototype
* @type {Function}
* @throws {TypeError} `this` must be a complex number array
* @returns {Iterator} iterator
*
* @example
* var Complex64 = require( '@stdlib/complex/float32' );
*
* var arr = [
*     new Complex64( 1.0, 1.0 ),
*     new Complex64( 2.0, 2.0 ),
*     new Complex64( 3.0, 3.0 )
* ];
* arr = new Complex64Array( arr );
*
* // Create an iterator:
* var it = arr.entries();
*
* // Iterate over the key-value pairs...
* var v = it.next().value;
* // returns [ 0, <Complex64> ]
*
* v = it.next().value;
* // returns [ 1, <Complex64> ]
*
* v = it.next().value;
* // returns [ 2, <Complex64> ]
*
* var bool = it.next().done;
* // returns true
*/
defineProperty( Complex64Array.prototype, 'entries', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': function entries() {
		var buffer;
		var self;
		var iter;
		var len;
		var FLG;
		var i;
		var j;
		if ( !isComplexArray( this ) ) {
			throw new TypeError( 'invalid invocation. `this` is not a complex number array.' );
		}
		self = this;
		buffer = this._buffer;
		len = this._length;

		// Initialize the iteration indices:
		i = -1;
		j = -2;

		// Create an iterator protocol-compliant object:
		iter = {};
		defineProperty( iter, 'next', {
			'configurable': false,
			'enumerable': false,
			'writable': false,
			'value': next
		});
		defineProperty( iter, 'return', {
			'configurable': false,
			'enumerable': false,
			'writable': false,
			'value': end
		});
		if ( ITERATOR_SYMBOL ) {
			defineProperty( iter, ITERATOR_SYMBOL, {
				'configurable': false,
				'enumerable': false,
				'writable': false,
				'value': factory
			});
		}
		return iter;

		/**
		* Returns an iterator protocol-compliant object containing the next iterated value.
		*
		* @private
		* @returns {Object} iterator protocol-compliant object
		*/
		function next() {
			var z;
			i += 1;
			if ( FLG || i >= len ) {
				return {
					'done': true
				};
			}
			j += 2;
			z = new Complex64( buffer[ j ], buffer[ j+1 ] );
			return {
				'value': [ i, z ],
				'done': false
			};
		}

		/**
		* Finishes an iterator.
		*
		* @private
		* @param {*} [value] - value to return
		* @returns {Object} iterator protocol-compliant object
		*/
		function end( value ) {
			FLG = true;
			if ( arguments.length ) {
				return {
					'value': value,
					'done': true
				};
			}
			return {
				'done': true
			};
		}

		/**
		* Returns a new iterator.
		*
		* @private
		* @returns {Iterator} iterator
		*/
		function factory() {
			return self.entries();
		}
	}
});

/**
* Returns an array element.
*
* @name get
* @memberof Complex64Array.prototype
* @type {Function}
* @param {ArrayLikeObject} [out] - output array
* @param {NonNegativeInteger} i - element index
* @throws {TypeError} `this` must be a complex number array
* @throws {TypeError} index argument must be a nonnegative integer
* @throws {TypeError} output argument must be an array-like object
* @returns {(Complex64|ArrayLikeObject|void)} array element
*
* @example
* var arr = new Complex64Array( 10 );
*
* var z = arr.get( 0 );
* // returns <Complex64>
*
* arr.set( [ 1.0, -1.0 ], 0 );
*
* z = arr.get( [ 0.0, 0.0 ], 0 );
* // returns [ 1.0, -1.0 ]
*
* z = arr.get( 100 );
* // returns undefined
*/
defineProperty( Complex64Array.prototype, 'get', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': function get( i ) {
		var idx;
		var out;
		var buf;

		if ( !isComplexArray( this ) ) {
			throw new TypeError( 'invalid invocation. `this` is not a complex number array.' );
		}
		buf = this._buffer;
		if ( arguments.length > 1 ) {
			idx = arguments[ 1 ];
			out = i;
			if ( !isArrayLikeObject( out ) || out.length < 2 ) {
				throw new TypeError( 'invalid argument. Output argument must be an array-like object. Value: `'+out+'`.' );
			}
		} else {
			idx = i;
		}
		if ( !isNonNegativeInteger( idx ) ) {
			throw new TypeError( 'invalid argument. Index argument must be a nonnegative integer. Value: `'+idx+'`.' );
		}
		if ( idx >= this._length ) {
			return;
		}
		idx *= 2;
		if ( out ) {
			out[ 0 ] = buf[ idx ];
			out[ 1 ] = buf[ idx+1 ];
			return out;
		}
		return new Complex64( buf[ idx ], buf[ idx+1 ] );
	}
});

/**
* Number of array elements.
*
* @name length
* @memberof Complex64Array.prototype
* @type {NonNegativeInteger}
*
* @example
* var arr = new Complex64Array( 10 );
*
* var len = arr.length;
* // returns 10
*/
defineProperty( Complex64Array.prototype, 'length', {
	'configurable': false,
	'enumerable': false,
	'get': function get() {
		return this._length;
	}
});

/**
* Sets an array element.
*
* ## Notes
*
* -   When provided a typed array, real or complex, we must check whether the source array shares the same buffer as the target array and whether the underlying memory overlaps. In particular, we are concerned with the following scenario:
*
*     ```text
*     buf:                ---------------------
*     src: ---------------------
*     ```
*
*     In the above, as we copy values from `src`, we will overwrite values in the `src` view, resulting in duplicated values copied into the end of `buf`, which is not intended. Hence, to avoid overwriting source values, we must **copy** source values to a temporary array.
*
*     In the other overlapping scenario,
*
*     ```text
*     buf: ---------------------
*     src:                ---------------------
*     ```
*
*     by the time we begin copying into the overlapping region, we are copying from the end of `src`, a non-overlapping region, which means we don't run the risk of copying copied values, rather than the original `src` values as intended.
*
*
* @name set
* @memberof Complex64Array.prototype
* @type {Function}
* @param {(Collection|Complex|ComplexArray)} value - value(s)
* @param {NonNegativeInteger} [i=0] - element index at which to start writing values
* @throws {TypeError} `this` must be a complex number array
* @throws {TypeError} first argument must be either a complex number, an array-like object, or a complex number array
* @throws {TypeError} index argument must be a nonnegative integer
* @throws {RangeError} array-like objects must have a length which is a multiple of two
* @throws {RangeError} index argument is out-of-bounds
* @throws {RangeError} target array lacks sufficient storage to accommodate source values
* @returns {void}
*
* @example
* var real = require( '@stdlib/complex/real' );
* var imag = require( '@stdlib/complex/imag' );
*
* var arr = new Complex64Array( 10 );
*
* var z = arr.get( 0 );
* // returns <Complex64>
*
* var re = real( z );
* // returns 0.0
*
* var im = imag( z );
* // returns 0.0
*
* arr.set( [ 1.0, -1.0 ], 0 );
*
* z = arr.get( 0 );
* // returns <Complex64>
*
* re = real( z );
* // returns 1.0
*
* im = imag( z );
* // returns -1.0
*/
defineProperty( Complex64Array.prototype, 'set', {
	'configurable': false,
	'enumerable': false,
	'writable': false,
	'value': function set( value ) {
		/* eslint-disable no-underscore-dangle */
		var sbuf;
		var idx;
		var buf;
		var tmp;
		var flg;
		var N;
		var v;
		var i;
		var j;
		if ( !isComplexArray( this ) ) {
			throw new TypeError( 'invalid invocation. `this` is not a complex number array.' );
		}
		buf = this._buffer;
		if ( arguments.length > 1 ) {
			idx = arguments[ 1 ];
			if ( !isNonNegativeInteger( idx ) ) {
				throw new TypeError( 'invalid argument. Index argument must be a nonnegative integer. Value: `'+idx+'`.' );
			}
		} else {
			idx = 0;
		}
		if ( isComplexLike( value ) ) {
			if ( idx >= this._length ) {
				throw new RangeError( 'invalid argument. Index argument is out-of-bounds. Value: `'+idx+'`.' );
			}
			idx *= 2;
			buf[ idx ] = real( value );
			buf[ idx+1 ] = imag( value );
			return;
		}
		if ( isComplexArray( value ) ) {
			N = value._length;
			if ( idx+N > this._length ) {
				throw new RangeError( 'invalid arguments. Target array lacks sufficient storage to accommodate source values.' );
			}
			sbuf = value._buffer;

			// Check for overlapping memory...
			j = buf.byteOffset + (idx*BYTES_PER_ELEMENT);
			if (
				sbuf.buffer === buf.buffer &&
				(
					sbuf.byteOffset < j &&
					sbuf.byteOffset+sbuf.byteLength > j
				)
			) {
				// We need to copy source values...
				tmp = new Float32Array( sbuf.length );
				for ( i = 0; i < sbuf.length; i++ ) {
					tmp[ i ] = sbuf[ i ];
				}
				sbuf = tmp;
			}
			idx *= 2;
			j = 0;
			for ( i = 0; i < N; i++ ) {
				buf[ idx ] = sbuf[ j ];
				buf[ idx+1 ] = sbuf[ j+1 ];
				idx += 2; // stride
				j += 2; // stride
			}
			return;
		}
		if ( isCollection( value ) ) {
			// Detect whether we've been provided an array of complex numbers...
			N = value.length;
			for ( i = 0; i < N; i++ ) {
				if ( !isComplexLike( value[ i ] ) ) {
					flg = true;
					break;
				}
			}
			// If an array does not contain only complex numbers, then we assume interleaved real and imaginary components...
			if ( flg ) {
				if ( !isEven( N ) ) {
					throw new RangeError( 'invalid argument. Array-like object arguments must have a length which is a multiple of two. Length: `'+N+'`.' );
				}
				if ( idx+(N/2) > this._length ) {
					throw new RangeError( 'invalid arguments. Target array lacks sufficient storage to accommodate source values.' );
				}
				sbuf = value;

				// Check for overlapping memory...
				j = buf.byteOffset + (idx*BYTES_PER_ELEMENT);
				if (
					sbuf.buffer === buf.buffer &&
					(
						sbuf.byteOffset < j &&
						sbuf.byteOffset+sbuf.byteLength > j
					)
				) {
					// We need to copy source values...
					tmp = new Float32Array( N );
					for ( i = 0; i < N; i++ ) {
						tmp[ i ] = sbuf[ i ];
					}
					sbuf = tmp;
				}
				idx *= 2;
				N /= 2;
				j = 0;
				for ( i = 0; i < N; i++ ) {
					buf[ idx ] = sbuf[ j ];
					buf[ idx+1 ] = sbuf[ j+1 ];
					idx += 2; // stride
					j += 2; // stride
				}
				return;
			}
			// If an array contains only complex numbers, then we need to extract real and imaginary components...
			if ( idx+N > this._length ) {
				throw new RangeError( 'invalid arguments. Target array lacks sufficient storage to accommodate source values.' );
			}
			idx *= 2;
			for ( i = 0; i < N; i++ ) {
				v = value[ i ];
				buf[ idx ] = real( v );
				buf[ idx+1 ] = imag( v );
				idx += 2; // stride
			}
			return;
		}
		throw new TypeError( 'invalid argument. First argument must be either a complex number, an array-like object, or a complex number array. Value: `'+value+'`.' );

		/* eslint-enable no-underscore-dangle */
	}
});


// EXPORTS //

module.exports = Complex64Array;

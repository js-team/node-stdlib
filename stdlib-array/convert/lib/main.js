/**
* @license Apache-2.0
*
* Copyright (c) 2018 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

'use strict';

// MODULES //

var isCollection = require( '@stdlib/assert/is-collection' );
var ctors = require( './../../ctors' );


// MAIN //

/**
* Converts an array to an array of a different data type.
*
* @param {Collection} x - array to convert
* @param {string} dtype - output data type
* @throws {TypeError} first argument must be an array-like object
* @throws {TypeError} second argument must be a recognized array data type
* @returns {(Array|TypedArray)} output array
*
* @example
* var arr = [ 1.0, 2.0, 3.0, 4.0 ];
* var out = convert( arr, 'float64' );
* // returns <Float64Array>[ 1.0, 2.0, 3.0, 4.0 ]
*/
function convert( x, dtype ) {
	var ctor;
	var out;
	var len;
	var i;
	if ( !isCollection( x ) ) {
		throw new TypeError( 'invalid argument. First argument must be an array-like object. Value: `' + x + '`.' );
	}
	len = x.length;
	ctor = ctors( dtype );
	if ( ctor === null ) {
		throw new TypeError( 'invalid argument. Second argument must be a recognized array data type. Value: `' + dtype + '`.' );
	}
	if ( dtype === 'generic' ) {
		out = [];
		for ( i = 0; i < len; i++ ) {
			out.push( x[ i ] ); // ensure "fast" elements
		}
		return out;
	}
	out = new ctor( len );
	for ( i = 0; i < len; i++ ) {
		out[ i ] = x[ i ];
	}
	return out;
}


// EXPORTS //

module.exports = convert;

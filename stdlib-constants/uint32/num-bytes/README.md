<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Number of Bytes

> Size (in bytes) of a 32-bit unsigned integer.

<section class="usage">

## Usage

```javascript
var UINT32_NUM_BYTES = require( '@stdlib/constants/uint32/num-bytes' );
```

#### UINT32_NUM_BYTES

Size (in bytes) of a 32-bit unsigned integer.

```javascript
var bool = ( UINT32_NUM_BYTES === 4 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var UINT32_NUM_BYTES = require( '@stdlib/constants/uint32/num-bytes' );

console.log( UINT32_NUM_BYTES );
// => 4
```

</section>

<!-- /.examples -->

<section class="links">

</section>

<!-- /.links -->

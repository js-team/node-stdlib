<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Max Uint16

> Maximum unsigned 16-bit integer.

<section class="usage">

## Usage

```javascript
var UINT16_MAX = require( '@stdlib/constants/uint16/max' );
```

#### UINT16_MAX

Maximum unsigned 16-bit integer.

```javascript
var bool = ( UINT16_MAX === 65535 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var UINT16_MAX = require( '@stdlib/constants/uint16/max' );

console.log( UINT16_MAX );
// => 65535
```

</section>

<!-- /.examples -->

<section class="links">

</section>

<!-- /.links -->

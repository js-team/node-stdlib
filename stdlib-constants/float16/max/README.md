<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Max Value

> Maximum [half-precision floating-point number][half-precision-floating-point-format].

<section class="usage">

## Usage

```javascript
var FLOAT16_MAX = require( '@stdlib/constants/float16/max' );
```

#### FLOAT16_MAX

Maximum [single-precision floating-point number][half-precision-floating-point-format].

```javascript
var bool = (FLOAT16_MAX === 65504.0 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var FLOAT16_MAX = require( '@stdlib/constants/float16/max' );

console.log( FLOAT16_MAX );
// => 65504.0
```

</section>

<!-- /.examples -->

<section class="links">

[half-precision-floating-point-format]: https://en.wikipedia.org/wiki/Half-precision_floating-point_format

</section>

<!-- /.links -->

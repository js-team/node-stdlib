<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# 2π

> The mathematical constant [π][@stdlib/constants/float64/pi] times 2.

<section class="usage">

## Usage

```javascript
var TWO_PI = require( '@stdlib/constants/float64/two-pi' );
```

#### TWO_PI

The mathematical constant [π][@stdlib/constants/float64/pi] times `2`.

```javascript
var bool = ( TWO_PI === 6.283185307179586 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var TWO_PI = require( '@stdlib/constants/float64/two-pi' );

console.log( TWO_PI );
// => 6.283185307179586
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/constants/float64/pi]: https://github.com/stdlib-js/constants/tree/main/float64/pi

</section>

<!-- /.links -->

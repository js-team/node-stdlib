<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Fourth Root of Epsilon

> [Fourth root][nth-root] of [double-precision floating-point epsilon][@stdlib/constants/float64/eps].

<section class="usage">

## Usage

```javascript
var FLOAT64_FOURTH_ROOT_EPS = require( '@stdlib/constants/float64/fourth-root-eps' );
```

#### FLOAT64_FOURTH_ROOT_EPS

[Fourth root][nth-root] of [double-precision floating-point epsilon][@stdlib/constants/float64/eps].

```javascript
var bool = ( FLOAT64_FOURTH_ROOT_EPS === 0.0001220703125 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var FLOAT64_FOURTH_ROOT_EPS = require( '@stdlib/constants/float64/fourth-root-eps' );

var out = FLOAT64_FOURTH_ROOT_EPS;
// returns 0.0001220703125
```

</section>

<!-- /.examples -->

<section class="links">

[nth-root]: https://en.wikipedia.org/wiki/Nth_root

[@stdlib/constants/float64/eps]: https://github.com/stdlib-js/constants/tree/main/float64/eps

</section>

<!-- /.links -->

<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Natural Logarithm of 10

> [Natural logarithm][@stdlib/math/base/special/ln] of `10`.

<section class="usage">

## Usage

```javascript
var LN10 = require( '@stdlib/constants/float64/ln-ten' );
```

#### LN10

[Natural logarithm][@stdlib/math/base/special/ln] of `10`.

```javascript
var bool = ( LN10 === 2.302585092994046 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var LN10 = require( '@stdlib/constants/float64/ln-ten' );

console.log( LN10 );
// => 2.302585092994046
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/math/base/special/ln]: https://github.com/stdlib-js/math-base-special-ln

</section>

<!-- /.links -->

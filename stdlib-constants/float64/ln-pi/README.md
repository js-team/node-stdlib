<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Natural logarithm of π

> Natural logarithm of the mathematical constant [π][pi].

<section class="usage">

## Usage

```javascript
var LN_PI = require( '@stdlib/constants/float64/ln-pi' );
```

#### LN_PI

Natural logarithm of the mathematical constant [π][pi].

```javascript
var bool = ( LN_PI === 1.1447298858494002 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var LN_PI = require( '@stdlib/constants/float64/ln-pi' );

console.log( LN_PI );
// => 1.1447298858494002
```

</section>

<!-- /.examples -->

<section class="links">

[pi]: https://en.wikipedia.org/wiki/Pi

</section>

<!-- /.links -->

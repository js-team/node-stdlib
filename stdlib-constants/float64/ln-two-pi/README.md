<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Natural Logarithm of 2π

> [Natural logarithm][@stdlib/math/base/special/ln] of `2π`.

<section class="usage">

## Usage

```javascript
var LN_TWO_PI = require( '@stdlib/constants/float64/ln-two-pi' );
```

#### LN_TWO_PI

[Natural logarithm][@stdlib/math/base/special/ln] of `2π`.

```javascript
var bool = ( LN_TWO_PI === 1.8378770664093456 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var LN_TWO_PI = require( '@stdlib/constants/float64/ln-two-pi' );

console.log( LN_TWO_PI );
// => 1.8378770664093456
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/math/base/special/ln]: https://github.com/stdlib-js/math-base-special-ln

</section>

<!-- /.links -->

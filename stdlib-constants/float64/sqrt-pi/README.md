<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Square Root of π

> Square root of the mathematical constant [π][@stdlib/constants/float64/pi].

<section class="usage">

## Usage

```javascript
var SQRT_PI = require( '@stdlib/constants/float64/sqrt-pi' );
```

#### SQRT_PI

Square root of the mathematical constant [π][@stdlib/constants/float64/pi].

```javascript
var bool = ( SQRT_PI === 1.7724538509055160 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var SQRT_PI = require( '@stdlib/constants/float64/sqrt-pi' );

console.log( SQRT_PI );
// => 1.7724538509055160
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/constants/float64/pi]: https://github.com/stdlib-js/constants/tree/main/float64/pi

</section>

<!-- /.links -->

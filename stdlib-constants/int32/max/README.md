<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Max Int32

> Maximum [signed 32-bit integer][max-int32].

<section class="usage">

## Usage

```javascript
var INT32_MAX = require( '@stdlib/constants/int32/max' );
```

#### INT32_MAX

Maximum [signed 32-bit integer][max-int32].

```javascript
var bool = ( INT32_MAX === 2147483647 );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better example -->

<!-- eslint no-undef: "error" -->

```javascript
var INT32_MAX = require( '@stdlib/constants/int32/max' );

console.log( INT32_MAX );
// => 2147483647
```

</section>

<!-- /.examples -->

<section class="links">

[max-int32]: http://en.wikipedia.org/wiki/2147483647

</section>

<!-- /.links -->

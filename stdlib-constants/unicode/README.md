<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# String Unicode Constants

> Standard library string unicode constants.

<section class="usage">

## Usage

```javascript
var constants = require( '@stdlib/constants/unicode' );
```

#### constants

Standard library string unicode constants.

```javascript
var ns = constants;
// returns {...}
```

<!-- <toc pattern="*"> -->

<div class="namespace-toc">

-   <span class="signature">[`MAX_BMP`][@stdlib/constants/unicode/max-bmp]</span><span class="delimiter">: </span><span class="description">maximum Unicode code point in the Basic Multilingual Plane (BMP).</span>
-   <span class="signature">[`MAX`][@stdlib/constants/unicode/max]</span><span class="delimiter">: </span><span class="description">maximum Unicode code point.</span>

</div>

<!-- </toc> -->

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better examples -->

<!-- eslint no-undef: "error" -->

```javascript
var objectKeys = require( '@stdlib/utils/keys' );
var constants = require( '@stdlib/constants/unicode' );

console.log( objectKeys( constants ) );
```

</section>

<!-- /.examples -->

<section class="links">

<!-- <toc-links> -->

[@stdlib/constants/unicode/max-bmp]: https://github.com/stdlib-js/constants/tree/main/unicode/max-bmp

[@stdlib/constants/unicode/max]: https://github.com/stdlib-js/constants/tree/main/unicode/max

<!-- </toc-links> -->

</section>

<!-- /.links -->

<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Machine Learning

[![NPM version][npm-image]][npm-url] [![Build Status][test-image]][test-url] [![Coverage Status][coverage-image]][coverage-url] [![dependencies][dependencies-image]][dependencies-url]

> Standard library machine learning algorithms.

<section class="installation">

## Installation

```bash
npm install @stdlib/ml
```

</section>

<section class="usage">

## Usage

```javascript
var ml = require( '@stdlib/ml' );
```

#### ml

Standard library machine learning algorithms.

```javascript
var algs = ml;
// returns {...}
```

The namespace contains the following sub-namespaces:

<!-- <toc pattern="*"> -->

<div class="namespace-toc">

-   <span class="signature">[`incr`][@stdlib/ml/incr]</span><span class="delimiter">: </span><span class="description">standard library incremental machine learning algorithms.</span>

</div>

<!-- </toc> -->

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better examples -->

<!-- eslint no-undef: "error" -->

```javascript
var objectKeys = require( '@stdlib/utils/keys' );
var ml = require( '@stdlib/ml' );

console.log( objectKeys( ml ) );
```

</section>

<!-- /.examples -->


<section class="main-repo" >

* * *

## Notice

This package is part of [stdlib][stdlib], a standard library for JavaScript and Node.js, with an emphasis on numerical and scientific computing. The library provides a collection of robust, high performance libraries for mathematics, statistics, streams, utilities, and more.

For more information on the project, filing bug reports and feature requests, and guidance on how to develop [stdlib][stdlib], see the main project [repository][stdlib].

#### Community

[![Chat][chat-image]][chat-url]

---

## License

See [LICENSE][stdlib-license].


## Copyright

Copyright &copy; 2016-2021. The Stdlib [Authors][stdlib-authors].

</section>

<!-- /.stdlib -->

<!-- Section for all links. Make sure to keep an empty line after the `section` element and another before the `/section` close. -->

<section class="links">

[npm-image]: http://img.shields.io/npm/v/@stdlib/ml.svg
[npm-url]: https://npmjs.org/package/@stdlib/ml

[test-image]: https://github.com/stdlib-js/ml/actions/workflows/test.yml/badge.svg
[test-url]: https://github.com/stdlib-js/ml/actions/workflows/test.yml

[coverage-image]: https://img.shields.io/codecov/c/github/stdlib-js/ml/main.svg
[coverage-url]: https://codecov.io/github/stdlib-js/ml?branch=main

[dependencies-image]: https://img.shields.io/david/stdlib-js/ml.svg
[dependencies-url]: https://david-dm.org/stdlib-js/ml/main

[chat-image]: https://img.shields.io/gitter/room/stdlib-js/stdlib.svg
[chat-url]: https://gitter.im/stdlib-js/stdlib/

[stdlib]: https://github.com/stdlib-js/stdlib

[stdlib-authors]: https://github.com/stdlib-js/stdlib/graphs/contributors

[stdlib-license]: https://raw.githubusercontent.com/stdlib-js/ml/main/LICENSE

<!-- <toc-links> -->

[@stdlib/ml/incr]: https://github.com/stdlib-js/ml/tree/main/incr

<!-- </toc-links> -->

</section>

<!-- /.links -->

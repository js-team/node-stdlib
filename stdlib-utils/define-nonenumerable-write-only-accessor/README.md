<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Non-Enumerable Write-Only Accessor

> [Define][@stdlib/utils/define-property] a non-enumerable **write-only** accessor.

<section class="usage">

## Usage

<!-- eslint-disable id-length -->

```javascript
var setNonEnumerableWriteOnlyAccessor = require( '@stdlib/utils/define-nonenumerable-write-only-accessor' );
```

#### setNonEnumerableWriteOnlyAccessor( obj, prop, setter )

[Defines][@stdlib/utils/define-property] a non-enumerable **write-only** accessor.

<!-- eslint-disable id-length -->

```javascript
var obj = {};
var val = '';

function setter( v ) {
    val = v;
}

setNonEnumerableWriteOnlyAccessor( obj, 'foo', setter );

obj.foo = 'boop';

var bool = ( val === 'boop' );
// returns true
```

</section>

<!-- /.usage -->

<section class="notes">
    
## Notes

-   Non-enumerable write-only accessors are **non-configurable**.

</section>

<!-- /.notes -->

<section class="examples">

## Examples

<!-- eslint-disable id-length -->

<!-- eslint no-undef: "error" -->

```javascript
var setNonEnumerableWriteOnlyAccessor = require( '@stdlib/utils/define-nonenumerable-write-only-accessor' );

function Foo( secret ) {
    if ( !(this instanceof Foo) ) {
        return new Foo( secret );
    }
    setNonEnumerableWriteOnlyAccessor( this, 'secret', setter );
    return this;

    function setter( v ) {
        secret = v;
    }
}

var foo = new Foo( 'beep' );

foo.secret = 'boop';
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/utils/define-property]: https://github.com/stdlib-js/utils/tree/main/define-property

</section>

<!-- /.links -->

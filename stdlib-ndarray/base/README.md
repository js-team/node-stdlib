<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Base

> Base ndarray.

<section class="usage">

## Usage

```javascript
var ns = require( '@stdlib/ndarray/base' );
```

#### ns

Base ndarray.

```javascript
var o = ns;
// returns {...}
```

<!-- <toc keywords="-assertion" > -->

<div class="namespace-toc">

-   <span class="signature">[`bind2vind( shape, strides, offset, order, idx, mode )`][@stdlib/ndarray/base/bind2vind]</span><span class="delimiter">: </span><span class="description">convert a linear index in an underlying data buffer to a linear index in an array view.</span>
-   <span class="signature">[`broadcastArray( arr, shape )`][@stdlib/ndarray/base/broadcast-array]</span><span class="delimiter">: </span><span class="description">broadcast an ndarray to a specified shape.</span>
-   <span class="signature">[`broadcastShapes( shapes )`][@stdlib/ndarray/base/broadcast-shapes]</span><span class="delimiter">: </span><span class="description">broadcast array shapes to a single shape.</span>
-   <span class="signature">[`bufferCtors( dtype )`][@stdlib/ndarray/base/buffer-ctors]</span><span class="delimiter">: </span><span class="description">ndarray data buffer constructors.</span>
-   <span class="signature">[`bufferDataTypeEnum( buffer )`][@stdlib/ndarray/base/buffer-dtype-enum]</span><span class="delimiter">: </span><span class="description">return the data type enumeration constant of an ndarray data buffer.</span>
-   <span class="signature">[`bufferDataType( buffer )`][@stdlib/ndarray/base/buffer-dtype]</span><span class="delimiter">: </span><span class="description">return the data type of an ndarray data buffer.</span>
-   <span class="signature">[`buffer( dtype, size )`][@stdlib/ndarray/base/buffer]</span><span class="delimiter">: </span><span class="description">create a zero-filled contiguous linear ndarray data buffer.</span>
-   <span class="signature">[`bytesPerElement( dtype )`][@stdlib/ndarray/base/bytes-per-element]</span><span class="delimiter">: </span><span class="description">return the number of bytes per element provided an underlying array data type.</span>
-   <span class="signature">[`clampIndex( idx, max )`][@stdlib/ndarray/base/clamp-index]</span><span class="delimiter">: </span><span class="description">restrict an index to the interval `[0,max]`.</span>
-   <span class="signature">[`ndarray( dtype, buffer, shape, strides, offset, order )`][@stdlib/ndarray/base/ctor]</span><span class="delimiter">: </span><span class="description">create a multidimensional array.</span>
-   <span class="signature">[`dtypeChar( dtype )`][@stdlib/ndarray/base/dtype-char]</span><span class="delimiter">: </span><span class="description">return the single letter abbreviation for an underlying array data type.</span>
-   <span class="signature">[`dtypes2signatures( dtypes, nin, nout )`][@stdlib/ndarray/base/dtypes2signatures]</span><span class="delimiter">: </span><span class="description">transform a list of array argument data types into a list of signatures.</span>
-   <span class="signature">[`ind( idx, max, mode )`][@stdlib/ndarray/base/ind]</span><span class="delimiter">: </span><span class="description">return an index given an index mode.</span>
-   <span class="signature">[`ind2sub( shape, strides, offset, order, idx, mode )`][@stdlib/ndarray/base/ind2sub]</span><span class="delimiter">: </span><span class="description">convert a linear index to an array of subscripts.</span>
-   <span class="signature">[`iterationOrder( strides )`][@stdlib/ndarray/base/iteration-order]</span><span class="delimiter">: </span><span class="description">given a stride array, determine array iteration order.</span>
-   <span class="signature">[`maxViewBufferIndex( shape, strides, offset )`][@stdlib/ndarray/base/max-view-buffer-index]</span><span class="delimiter">: </span><span class="description">compute the maximum linear index in an underlying data buffer accessible to an array view.</span>
-   <span class="signature">[`minViewBufferIndex( shape, strides, offset )`][@stdlib/ndarray/base/min-view-buffer-index]</span><span class="delimiter">: </span><span class="description">compute the minimum linear index in an underlying data buffer accessible to an array view.</span>
-   <span class="signature">[`minmaxViewBufferIndex( shape, strides, offset )`][@stdlib/ndarray/base/minmax-view-buffer-index]</span><span class="delimiter">: </span><span class="description">compute the minimum and maximum linear indices in an underlying data buffer which are accessible to an array view.</span>
-   <span class="signature">[`nonsingletonDimensions( shape )`][@stdlib/ndarray/base/nonsingleton-dimensions]</span><span class="delimiter">: </span><span class="description">return the number of non-singleton dimensions.</span>
-   <span class="signature">[`numel( shape )`][@stdlib/ndarray/base/numel]</span><span class="delimiter">: </span><span class="description">return the number of elements in an array.</span>
-   <span class="signature">[`serializeMetaData( x )`][@stdlib/ndarray/base/serialize-meta-data]</span><span class="delimiter">: </span><span class="description">serialize ndarray meta data.</span>
-   <span class="signature">[`shape2strides( shape, order )`][@stdlib/ndarray/base/shape2strides]</span><span class="delimiter">: </span><span class="description">generate a stride array from an array shape.</span>
-   <span class="signature">[`singletonDimensions( shape )`][@stdlib/ndarray/base/singleton-dimensions]</span><span class="delimiter">: </span><span class="description">return the number of singleton dimensions.</span>
-   <span class="signature">[`strides2offset( shape, strides )`][@stdlib/ndarray/base/strides2offset]</span><span class="delimiter">: </span><span class="description">determine the index offset which specifies the location of the first indexed value in a multidimensional array based on a stride array.</span>
-   <span class="signature">[`strides2order( strides )`][@stdlib/ndarray/base/strides2order]</span><span class="delimiter">: </span><span class="description">determine the order of a multidimensional array based on a provided stride array.</span>
-   <span class="signature">[`sub2ind( shape, strides, offset, ...subscripts, mode )`][@stdlib/ndarray/base/sub2ind]</span><span class="delimiter">: </span><span class="description">convert subscripts to a linear index.</span>
-   <span class="signature">[`ndarray2array( buffer, shape, strides, offset, order )`][@stdlib/ndarray/base/to-array]</span><span class="delimiter">: </span><span class="description">convert an ndarray buffer to a generic array.</span>
-   <span class="signature">[`vind2bind( shape, strides, offset, order, idx, mode )`][@stdlib/ndarray/base/vind2bind]</span><span class="delimiter">: </span><span class="description">convert a linear index in an array view to a linear index in an underlying data buffer.</span>
-   <span class="signature">[`wrapIndex( idx, max )`][@stdlib/ndarray/base/wrap-index]</span><span class="delimiter">: </span><span class="description">wrap an index on the interval `[0,max]`.</span>

</div>

<!-- </toc> -->

The namespace contains the following sub-namespaces:

<!-- <toc pattern="*"> -->

<div class="namespace-toc">

-   <span class="signature">[`assert`][@stdlib/ndarray/base/assert]</span><span class="delimiter">: </span><span class="description">base ndarray assertion utilities.</span>

</div>

<!-- </toc> -->

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better examples -->

<!-- eslint no-undef: "error" -->

```javascript
var objectKeys = require( '@stdlib/utils/keys' );
var ns = require( '@stdlib/ndarray/base' );

console.log( objectKeys( ns ) );
```

</section>

<!-- /.examples -->

<section class="links">

<!-- <toc-links> -->

[@stdlib/ndarray/base/assert]: https://github.com/stdlib-js/ndarray/tree/main/base/assert

[@stdlib/ndarray/base/bind2vind]: https://github.com/stdlib-js/ndarray/tree/main/base/bind2vind

[@stdlib/ndarray/base/broadcast-array]: https://github.com/stdlib-js/ndarray/tree/main/base/broadcast-array

[@stdlib/ndarray/base/broadcast-shapes]: https://github.com/stdlib-js/ndarray/tree/main/base/broadcast-shapes

[@stdlib/ndarray/base/buffer-ctors]: https://github.com/stdlib-js/ndarray/tree/main/base/buffer-ctors

[@stdlib/ndarray/base/buffer-dtype-enum]: https://github.com/stdlib-js/ndarray/tree/main/base/buffer-dtype-enum

[@stdlib/ndarray/base/buffer-dtype]: https://github.com/stdlib-js/ndarray/tree/main/base/buffer-dtype

[@stdlib/ndarray/base/buffer]: https://github.com/stdlib-js/ndarray/tree/main/base/buffer

[@stdlib/ndarray/base/bytes-per-element]: https://github.com/stdlib-js/ndarray/tree/main/base/bytes-per-element

[@stdlib/ndarray/base/clamp-index]: https://github.com/stdlib-js/ndarray/tree/main/base/clamp-index

[@stdlib/ndarray/base/ctor]: https://github.com/stdlib-js/ndarray/tree/main/base/ctor

[@stdlib/ndarray/base/dtype-char]: https://github.com/stdlib-js/ndarray/tree/main/base/dtype-char

[@stdlib/ndarray/base/dtypes2signatures]: https://github.com/stdlib-js/ndarray/tree/main/base/dtypes2signatures

[@stdlib/ndarray/base/ind]: https://github.com/stdlib-js/ndarray/tree/main/base/ind

[@stdlib/ndarray/base/ind2sub]: https://github.com/stdlib-js/ndarray/tree/main/base/ind2sub

[@stdlib/ndarray/base/iteration-order]: https://github.com/stdlib-js/ndarray/tree/main/base/iteration-order

[@stdlib/ndarray/base/max-view-buffer-index]: https://github.com/stdlib-js/ndarray/tree/main/base/max-view-buffer-index

[@stdlib/ndarray/base/min-view-buffer-index]: https://github.com/stdlib-js/ndarray/tree/main/base/min-view-buffer-index

[@stdlib/ndarray/base/minmax-view-buffer-index]: https://github.com/stdlib-js/ndarray/tree/main/base/minmax-view-buffer-index

[@stdlib/ndarray/base/nonsingleton-dimensions]: https://github.com/stdlib-js/ndarray/tree/main/base/nonsingleton-dimensions

[@stdlib/ndarray/base/numel]: https://github.com/stdlib-js/ndarray/tree/main/base/numel

[@stdlib/ndarray/base/serialize-meta-data]: https://github.com/stdlib-js/ndarray/tree/main/base/serialize-meta-data

[@stdlib/ndarray/base/shape2strides]: https://github.com/stdlib-js/ndarray/tree/main/base/shape2strides

[@stdlib/ndarray/base/singleton-dimensions]: https://github.com/stdlib-js/ndarray/tree/main/base/singleton-dimensions

[@stdlib/ndarray/base/strides2offset]: https://github.com/stdlib-js/ndarray/tree/main/base/strides2offset

[@stdlib/ndarray/base/strides2order]: https://github.com/stdlib-js/ndarray/tree/main/base/strides2order

[@stdlib/ndarray/base/sub2ind]: https://github.com/stdlib-js/ndarray/tree/main/base/sub2ind

[@stdlib/ndarray/base/to-array]: https://github.com/stdlib-js/ndarray/tree/main/base/to-array

[@stdlib/ndarray/base/vind2bind]: https://github.com/stdlib-js/ndarray/tree/main/base/vind2bind

[@stdlib/ndarray/base/wrap-index]: https://github.com/stdlib-js/ndarray/tree/main/base/wrap-index

<!-- </toc-links> -->

</section>

<!-- /.links -->

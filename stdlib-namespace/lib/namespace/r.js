/* eslint-disable max-lines */

/**
* @license Apache-2.0
*
* Copyright (c) 2018 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

'use strict';

/*
* When adding names to the namespace, ensure that they are added in alphabetical order according to alias (namespace key).
*/

var ns = [];

ns.push({
	'alias': 'random.iterators.arcsine',
	'path': '@stdlib/random/iter/arcsine',
	'value': require( '@stdlib/random/iter/arcsine' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/arcsine'
	]
});

ns.push({
	'alias': 'random.iterators.bernoulli',
	'path': '@stdlib/random/iter/bernoulli',
	'value': require( '@stdlib/random/iter/bernoulli' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/bernoulli'
	]
});

ns.push({
	'alias': 'random.iterators.beta',
	'path': '@stdlib/random/iter/beta',
	'value': require( '@stdlib/random/iter/beta' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/beta'
	]
});

ns.push({
	'alias': 'random.iterators.betaprime',
	'path': '@stdlib/random/iter/betaprime',
	'value': require( '@stdlib/random/iter/betaprime' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/betaprime'
	]
});

ns.push({
	'alias': 'random.iterators.binomial',
	'path': '@stdlib/random/iter/binomial',
	'value': require( '@stdlib/random/iter/binomial' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/binomial'
	]
});

ns.push({
	'alias': 'random.iterators.boxMuller',
	'path': '@stdlib/random/iter/box-muller',
	'value': require( '@stdlib/random/iter/box-muller' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/box-muller'
	]
});

ns.push({
	'alias': 'random.iterators.cauchy',
	'path': '@stdlib/random/iter/cauchy',
	'value': require( '@stdlib/random/iter/cauchy' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/cauchy'
	]
});

ns.push({
	'alias': 'random.iterators.chi',
	'path': '@stdlib/random/iter/chi',
	'value': require( '@stdlib/random/iter/chi' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/chi'
	]
});

ns.push({
	'alias': 'random.iterators.chisquare',
	'path': '@stdlib/random/iter/chisquare',
	'value': require( '@stdlib/random/iter/chisquare' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/chisquare'
	]
});

ns.push({
	'alias': 'random.iterators.cosine',
	'path': '@stdlib/random/iter/cosine',
	'value': require( '@stdlib/random/iter/cosine' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/cosine'
	]
});

ns.push({
	'alias': 'random.iterators.discreteUniform',
	'path': '@stdlib/random/iter/discrete-uniform',
	'value': require( '@stdlib/random/iter/discrete-uniform' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/discrete-uniform'
	]
});

ns.push({
	'alias': 'random.iterators.erlang',
	'path': '@stdlib/random/iter/erlang',
	'value': require( '@stdlib/random/iter/erlang' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/erlang'
	]
});

ns.push({
	'alias': 'random.iterators.exponential',
	'path': '@stdlib/random/iter/exponential',
	'value': require( '@stdlib/random/iter/exponential' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/exponential'
	]
});

ns.push({
	'alias': 'random.iterators.f',
	'path': '@stdlib/random/iter/f',
	'value': require( '@stdlib/random/iter/f' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/f'
	]
});

ns.push({
	'alias': 'random.iterators.frechet',
	'path': '@stdlib/random/iter/frechet',
	'value': require( '@stdlib/random/iter/frechet' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/frechet'
	]
});

ns.push({
	'alias': 'random.iterators.gamma',
	'path': '@stdlib/random/iter/gamma',
	'value': require( '@stdlib/random/iter/gamma' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/gamma'
	]
});

ns.push({
	'alias': 'random.iterators.geometric',
	'path': '@stdlib/random/iter/geometric',
	'value': require( '@stdlib/random/iter/geometric' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/geometric'
	]
});

ns.push({
	'alias': 'random.iterators.gumbel',
	'path': '@stdlib/random/iter/gumbel',
	'value': require( '@stdlib/random/iter/gumbel' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/gumbel'
	]
});

ns.push({
	'alias': 'random.iterators.hypergeometric',
	'path': '@stdlib/random/iter/hypergeometric',
	'value': require( '@stdlib/random/iter/hypergeometric' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/hypergeometric'
	]
});

ns.push({
	'alias': 'random.iterators.improvedZiggurat',
	'path': '@stdlib/random/iter/improved-ziggurat',
	'value': require( '@stdlib/random/iter/improved-ziggurat' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/improved-ziggurat'
	]
});

ns.push({
	'alias': 'random.iterators.invgamma',
	'path': '@stdlib/random/iter/invgamma',
	'value': require( '@stdlib/random/iter/invgamma' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/invgamma'
	]
});

ns.push({
	'alias': 'random.iterators.kumaraswamy',
	'path': '@stdlib/random/iter/kumaraswamy',
	'value': require( '@stdlib/random/iter/kumaraswamy' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/kumaraswamy'
	]
});

ns.push({
	'alias': 'random.iterators.laplace',
	'path': '@stdlib/random/iter/laplace',
	'value': require( '@stdlib/random/iter/laplace' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/laplace'
	]
});

ns.push({
	'alias': 'random.iterators.levy',
	'path': '@stdlib/random/iter/levy',
	'value': require( '@stdlib/random/iter/levy' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/levy'
	]
});

ns.push({
	'alias': 'random.iterators.logistic',
	'path': '@stdlib/random/iter/logistic',
	'value': require( '@stdlib/random/iter/logistic' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/logistic'
	]
});

ns.push({
	'alias': 'random.iterators.lognormal',
	'path': '@stdlib/random/iter/lognormal',
	'value': require( '@stdlib/random/iter/lognormal' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/lognormal'
	]
});

ns.push({
	'alias': 'random.iterators.minstd',
	'path': '@stdlib/random/iter/minstd',
	'value': require( '@stdlib/random/iter/minstd' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/minstd',
		'@stdlib/random/iter/minstd-shuffle',
		'@stdlib/random/iter/mt19937',
		'@stdlib/random/iter/randi',
		'@stdlib/random/iter/randu'
	]
});

ns.push({
	'alias': 'random.iterators.minstdShuffle',
	'path': '@stdlib/random/iter/minstd-shuffle',
	'value': require( '@stdlib/random/iter/minstd-shuffle' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/minstd-shuffle',
		'@stdlib/random/iter/minstd',
		'@stdlib/random/iter/mt19937',
		'@stdlib/random/iter/randi',
		'@stdlib/random/iter/randu'
	]
});

ns.push({
	'alias': 'random.iterators.mt19937',
	'path': '@stdlib/random/iter/mt19937',
	'value': require( '@stdlib/random/iter/mt19937' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/mt19937',
		'@stdlib/random/iter/minstd',
		'@stdlib/random/iter/minstd-shuffle',
		'@stdlib/random/iter/randi',
		'@stdlib/random/iter/randu'
	]
});

ns.push({
	'alias': 'random.iterators.negativeBinomial',
	'path': '@stdlib/random/iter/negative-binomial',
	'value': require( '@stdlib/random/iter/negative-binomial' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/negative-binomial'
	]
});

ns.push({
	'alias': 'random.iterators.normal',
	'path': '@stdlib/random/iter/normal',
	'value': require( '@stdlib/random/iter/normal' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/normal'
	]
});

ns.push({
	'alias': 'random.iterators.pareto1',
	'path': '@stdlib/random/iter/pareto-type1',
	'value': require( '@stdlib/random/iter/pareto-type1' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/pareto-type1'
	]
});

ns.push({
	'alias': 'random.iterators.poisson',
	'path': '@stdlib/random/iter/poisson',
	'value': require( '@stdlib/random/iter/poisson' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/poisson'
	]
});

ns.push({
	'alias': 'random.iterators.randi',
	'path': '@stdlib/random/iter/randi',
	'value': require( '@stdlib/random/iter/randi' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/randi',
		'@stdlib/random/iter/randu'
	]
});

ns.push({
	'alias': 'random.iterators.randn',
	'path': '@stdlib/random/iter/randn',
	'value': require( '@stdlib/random/iter/randn' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/randn'
	]
});

ns.push({
	'alias': 'random.iterators.randu',
	'path': '@stdlib/random/iter/randu',
	'value': require( '@stdlib/random/iter/randu' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/randu',
		'@stdlib/random/iter/randi'
	]
});

ns.push({
	'alias': 'random.iterators.rayleigh',
	'path': '@stdlib/random/iter/rayleigh',
	'value': require( '@stdlib/random/iter/rayleigh' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/rayleigh'
	]
});

ns.push({
	'alias': 'random.iterators.t',
	'path': '@stdlib/random/iter/t',
	'value': require( '@stdlib/random/iter/t' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/t'
	]
});

ns.push({
	'alias': 'random.iterators.triangular',
	'path': '@stdlib/random/iter/triangular',
	'value': require( '@stdlib/random/iter/triangular' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/triangular'
	]
});

ns.push({
	'alias': 'random.iterators.uniform',
	'path': '@stdlib/random/iter/uniform',
	'value': require( '@stdlib/random/iter/uniform' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/uniform'
	]
});

ns.push({
	'alias': 'random.iterators.weibull',
	'path': '@stdlib/random/iter/weibull',
	'value': require( '@stdlib/random/iter/weibull' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/weibull'
	]
});

ns.push({
	'alias': 'random.streams.arcsine',
	'path': '@stdlib/random/streams/arcsine',
	'value': require( '@stdlib/random/streams/arcsine' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/arcsine',
		'@stdlib/random/iter/arcsine'
	]
});

ns.push({
	'alias': 'random.streams.bernoulli',
	'path': '@stdlib/random/streams/bernoulli',
	'value': require( '@stdlib/random/streams/bernoulli' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/bernoulli',
		'@stdlib/random/iter/bernoulli'
	]
});

ns.push({
	'alias': 'random.streams.beta',
	'path': '@stdlib/random/streams/beta',
	'value': require( '@stdlib/random/streams/beta' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/beta',
		'@stdlib/random/iter/beta'
	]
});

ns.push({
	'alias': 'random.streams.betaprime',
	'path': '@stdlib/random/streams/betaprime',
	'value': require( '@stdlib/random/streams/betaprime' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/betaprime',
		'@stdlib/random/iter/betaprime'
	]
});

ns.push({
	'alias': 'random.streams.binomial',
	'path': '@stdlib/random/streams/binomial',
	'value': require( '@stdlib/random/streams/binomial' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/binomial',
		'@stdlib/random/iter/binomial'
	]
});

ns.push({
	'alias': 'random.streams.boxMuller',
	'path': '@stdlib/random/streams/box-muller',
	'value': require( '@stdlib/random/streams/box-muller' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/box-muller',
		'@stdlib/random/iter/box-muller',
		'@stdlib/random/streams/improved-ziggurat',
		'@stdlib/random/streams/randn'
	]
});

ns.push({
	'alias': 'random.streams.cauchy',
	'path': '@stdlib/random/streams/cauchy',
	'value': require( '@stdlib/random/streams/cauchy' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/cauchy',
		'@stdlib/random/iter/cauchy'
	]
});

ns.push({
	'alias': 'random.streams.chi',
	'path': '@stdlib/random/streams/chi',
	'value': require( '@stdlib/random/streams/chi' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/chi',
		'@stdlib/random/iter/chi'
	]
});

ns.push({
	'alias': 'random.streams.chisquare',
	'path': '@stdlib/random/streams/chisquare',
	'value': require( '@stdlib/random/streams/chisquare' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/chisquare',
		'@stdlib/random/iter/chisquare'
	]
});

ns.push({
	'alias': 'random.streams.cosine',
	'path': '@stdlib/random/streams/cosine',
	'value': require( '@stdlib/random/streams/cosine' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/cosine',
		'@stdlib/random/iter/cosine'
	]
});

ns.push({
	'alias': 'random.streams.discreteUniform',
	'path': '@stdlib/random/streams/discrete-uniform',
	'value': require( '@stdlib/random/streams/discrete-uniform' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/discrete-uniform',
		'@stdlib/random/iter/discrete-uniform'
	]
});

ns.push({
	'alias': 'random.streams.erlang',
	'path': '@stdlib/random/streams/erlang',
	'value': require( '@stdlib/random/streams/erlang' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/erlang',
		'@stdlib/random/iter/erlang'
	]
});

ns.push({
	'alias': 'random.streams.exponential',
	'path': '@stdlib/random/streams/exponential',
	'value': require( '@stdlib/random/streams/exponential' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/exponential',
		'@stdlib/random/iter/exponential'
	]
});

ns.push({
	'alias': 'random.streams.f',
	'path': '@stdlib/random/streams/f',
	'value': require( '@stdlib/random/streams/f' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/f',
		'@stdlib/random/iter/f'
	]
});

ns.push({
	'alias': 'random.streams.frechet',
	'path': '@stdlib/random/streams/frechet',
	'value': require( '@stdlib/random/streams/frechet' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/frechet',
		'@stdlib/random/iter/frechet'
	]
});

ns.push({
	'alias': 'random.streams.gamma',
	'path': '@stdlib/random/streams/gamma',
	'value': require( '@stdlib/random/streams/gamma' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/gamma',
		'@stdlib/random/iter/gamma'
	]
});

ns.push({
	'alias': 'random.streams.geometric',
	'path': '@stdlib/random/streams/geometric',
	'value': require( '@stdlib/random/streams/geometric' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/geometric',
		'@stdlib/random/iter/geometric'
	]
});

ns.push({
	'alias': 'random.streams.gumbel',
	'path': '@stdlib/random/streams/gumbel',
	'value': require( '@stdlib/random/streams/gumbel' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/gumbel',
		'@stdlib/random/iter/gumbel'
	]
});

ns.push({
	'alias': 'random.streams.hypergeometric',
	'path': '@stdlib/random/streams/hypergeometric',
	'value': require( '@stdlib/random/streams/hypergeometric' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/hypergeometric',
		'@stdlib/random/iter/hypergeometric'
	]
});

ns.push({
	'alias': 'random.streams.improvedZiggurat',
	'path': '@stdlib/random/streams/improved-ziggurat',
	'value': require( '@stdlib/random/streams/improved-ziggurat' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/improved-ziggurat',
		'@stdlib/random/iter/improved-ziggurat',
		'@stdlib/random/streams/box-muller',
		'@stdlib/random/streams/randn'
	]
});

ns.push({
	'alias': 'random.streams.invgamma',
	'path': '@stdlib/random/streams/invgamma',
	'value': require( '@stdlib/random/streams/invgamma' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/invgamma',
		'@stdlib/random/iter/invgamma'
	]
});

ns.push({
	'alias': 'random.streams.kumaraswamy',
	'path': '@stdlib/random/streams/kumaraswamy',
	'value': require( '@stdlib/random/streams/kumaraswamy' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/kumaraswamy',
		'@stdlib/random/iter/kumaraswamy'
	]
});

ns.push({
	'alias': 'random.streams.laplace',
	'path': '@stdlib/random/streams/laplace',
	'value': require( '@stdlib/random/streams/laplace' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/laplace',
		'@stdlib/random/iter/laplace'
	]
});

ns.push({
	'alias': 'random.streams.levy',
	'path': '@stdlib/random/streams/levy',
	'value': require( '@stdlib/random/streams/levy' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/levy',
		'@stdlib/random/iter/levy'
	]
});

ns.push({
	'alias': 'random.streams.logistic',
	'path': '@stdlib/random/streams/logistic',
	'value': require( '@stdlib/random/streams/logistic' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/logistic',
		'@stdlib/random/iter/logistic'
	]
});

ns.push({
	'alias': 'random.streams.lognormal',
	'path': '@stdlib/random/streams/lognormal',
	'value': require( '@stdlib/random/streams/lognormal' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/lognormal',
		'@stdlib/random/iter/lognormal'
	]
});

ns.push({
	'alias': 'random.streams.minstd',
	'path': '@stdlib/random/streams/minstd',
	'value': require( '@stdlib/random/streams/minstd' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/minstd',
		'@stdlib/random/iter/minstd',
		'@stdlib/random/streams/minstd-shuffle',
		'@stdlib/random/streams/mt19937',
		'@stdlib/random/streams/randi',
		'@stdlib/random/streams/randu'
	]
});

ns.push({
	'alias': 'random.streams.minstdShuffle',
	'path': '@stdlib/random/streams/minstd-shuffle',
	'value': require( '@stdlib/random/streams/minstd-shuffle' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/minstd-shuffle',
		'@stdlib/random/iter/minstd-shuffle',
		'@stdlib/random/streams/minstd',
		'@stdlib/random/streams/mt19937',
		'@stdlib/random/streams/randi',
		'@stdlib/random/streams/randu'
	]
});

ns.push({
	'alias': 'random.streams.mt19937',
	'path': '@stdlib/random/streams/mt19937',
	'value': require( '@stdlib/random/streams/mt19937' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/mt19937',
		'@stdlib/random/iter/mt19937',
		'@stdlib/random/streams/minstd',
		'@stdlib/random/streams/minstd-shuffle',
		'@stdlib/random/streams/randi',
		'@stdlib/random/streams/randu'
	]
});

ns.push({
	'alias': 'random.streams.negativeBinomial',
	'path': '@stdlib/random/streams/negative-binomial',
	'value': require( '@stdlib/random/streams/negative-binomial' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/negative-binomial',
		'@stdlib/random/iter/negative-binomial'
	]
});

ns.push({
	'alias': 'random.streams.normal',
	'path': '@stdlib/random/streams/normal',
	'value': require( '@stdlib/random/streams/normal' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/normal',
		'@stdlib/random/iter/normal'
	]
});

ns.push({
	'alias': 'random.streams.pareto1',
	'path': '@stdlib/random/streams/pareto-type1',
	'value': require( '@stdlib/random/streams/pareto-type1' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/pareto-type1',
		'@stdlib/random/iter/pareto-type1'
	]
});

ns.push({
	'alias': 'random.streams.poisson',
	'path': '@stdlib/random/streams/poisson',
	'value': require( '@stdlib/random/streams/poisson' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/poisson',
		'@stdlib/random/iter/poisson'
	]
});

ns.push({
	'alias': 'random.streams.randi',
	'path': '@stdlib/random/streams/randi',
	'value': require( '@stdlib/random/streams/randi' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/randi',
		'@stdlib/random/iter/randi',
		'@stdlib/random/streams/randu'
	]
});

ns.push({
	'alias': 'random.streams.randn',
	'path': '@stdlib/random/streams/randn',
	'value': require( '@stdlib/random/streams/randn' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/randn',
		'@stdlib/random/iter/randn',
		'@stdlib/random/streams/box-muller',
		'@stdlib/random/streams/improved-ziggurat'
	]
});

ns.push({
	'alias': 'random.streams.randu',
	'path': '@stdlib/random/streams/randu',
	'value': require( '@stdlib/random/streams/randu' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/randu',
		'@stdlib/random/iter/randu',
		'@stdlib/random/streams/randi'
	]
});

ns.push({
	'alias': 'random.streams.rayleigh',
	'path': '@stdlib/random/streams/rayleigh',
	'value': require( '@stdlib/random/streams/rayleigh' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/rayleigh',
		'@stdlib/random/iter/rayleigh'
	]
});

ns.push({
	'alias': 'random.streams.t',
	'path': '@stdlib/random/streams/t',
	'value': require( '@stdlib/random/streams/t' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/t',
		'@stdlib/random/iter/t'
	]
});

ns.push({
	'alias': 'random.streams.triangular',
	'path': '@stdlib/random/streams/triangular',
	'value': require( '@stdlib/random/streams/triangular' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/triangular',
		'@stdlib/random/iter/triangular'
	]
});

ns.push({
	'alias': 'random.streams.uniform',
	'path': '@stdlib/random/streams/uniform',
	'value': require( '@stdlib/random/streams/uniform' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/uniform',
		'@stdlib/random/iter/uniform'
	]
});

ns.push({
	'alias': 'random.streams.weibull',
	'path': '@stdlib/random/streams/weibull',
	'value': require( '@stdlib/random/streams/weibull' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/base/weibull',
		'@stdlib/random/iter/weibull'
	]
});

ns.push({
	'alias': 'ranks',
	'path': '@stdlib/stats/ranks',
	'value': require( '@stdlib/stats/ranks' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'readDir',
	'path': '@stdlib/fs/read-dir',
	'value': require( '@stdlib/fs/read-dir' ),
	'type': 'Function',
	'related': [
		'@stdlib/fs/exists',
		'@stdlib/fs/read-file'
	]
});

ns.push({
	'alias': 'readFile',
	'path': '@stdlib/fs/read-file',
	'value': require( '@stdlib/fs/read-file' ),
	'type': 'Function',
	'related': [
		'@stdlib/fs/exists',
		'@stdlib/fs/open',
		'@stdlib/fs/read-dir',
		'@stdlib/fs/read-json',
		'@stdlib/fs/write-file'
	]
});

ns.push({
	'alias': 'readFileList',
	'path': '@stdlib/fs/read-file-list',
	'value': require( '@stdlib/fs/read-file-list' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'readJSON',
	'path': '@stdlib/fs/read-json',
	'value': require( '@stdlib/fs/read-json' ),
	'type': 'Function',
	'related': [
		'@stdlib/fs/read-file'
	]
});

ns.push({
	'alias': 'readWASM',
	'path': '@stdlib/fs/read-wasm',
	'value': require( '@stdlib/fs/read-wasm' ),
	'type': 'Function',
	'related': [
		'@stdlib/fs/read-file'
	]
});

ns.push({
	'alias': 'real',
	'path': '@stdlib/complex/real',
	'value': require( '@stdlib/complex/real' ),
	'type': 'Function',
	'related': [
		'@stdlib/complex/imag',
		'@stdlib/complex/reim'
	]
});

ns.push({
	'alias': 'realmax',
	'path': '@stdlib/utils/real-max',
	'value': require( '@stdlib/utils/real-max' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/real-min',
		'@stdlib/utils/type-max'
	]
});

ns.push({
	'alias': 'realmin',
	'path': '@stdlib/utils/real-min',
	'value': require( '@stdlib/utils/real-min' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/real-max',
		'@stdlib/utils/type-min'
	]
});

ns.push({
	'alias': 'reBasename',
	'path': '@stdlib/regexp/basename',
	'value': require( '@stdlib/regexp/basename' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/basename-posix',
		'@stdlib/regexp/basename-windows'
	]
});

ns.push({
	'alias': 'reBasenamePosix',
	'path': '@stdlib/regexp/basename-posix',
	'value': require( '@stdlib/regexp/basename-posix' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/basename',
		'@stdlib/regexp/basename-windows'
	]
});

ns.push({
	'alias': 'reBasenameWindows',
	'path': '@stdlib/regexp/basename-windows',
	'value': require( '@stdlib/regexp/basename-windows' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/basename',
		'@stdlib/regexp/basename-posix'
	]
});

ns.push({
	'alias': 'reColorHexadecimal',
	'path': '@stdlib/regexp/color-hexadecimal',
	'value': require( '@stdlib/regexp/color-hexadecimal' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'reDecimalNumber',
	'path': '@stdlib/regexp/decimal-number',
	'value': require( '@stdlib/regexp/decimal-number' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'reDirname',
	'path': '@stdlib/regexp/dirname',
	'value': require( '@stdlib/regexp/dirname' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/dirname-posix',
		'@stdlib/regexp/dirname-windows',
		'@stdlib/utils/dirname'
	]
});

ns.push({
	'alias': 'reDirnamePosix',
	'path': '@stdlib/regexp/dirname-posix',
	'value': require( '@stdlib/regexp/dirname-posix' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/dirname',
		'@stdlib/regexp/dirname-windows',
		'@stdlib/utils/dirname'
	]
});

ns.push({
	'alias': 'reDirnameWindows',
	'path': '@stdlib/regexp/dirname-windows',
	'value': require( '@stdlib/regexp/dirname-windows' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/dirname',
		'@stdlib/regexp/dirname-posix',
		'@stdlib/utils/dirname'
	]
});

ns.push({
	'alias': 'reduce',
	'path': '@stdlib/utils/reduce',
	'value': require( '@stdlib/utils/reduce' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/for-each',
		'@stdlib/utils/map',
		'@stdlib/utils/async/reduce',
		'@stdlib/utils/reduce-right'
	]
});

ns.push({
	'alias': 'reduceAsync',
	'path': '@stdlib/utils/async/reduce',
	'value': require( '@stdlib/utils/async/reduce' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/async/for-each',
		'@stdlib/utils/async/map',
		'@stdlib/utils/reduce',
		'@stdlib/utils/async/reduce-right'
	]
});

ns.push({
	'alias': 'reduceRight',
	'path': '@stdlib/utils/reduce-right',
	'value': require( '@stdlib/utils/reduce-right' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/for-each-right',
		'@stdlib/utils/map-right',
		'@stdlib/utils/reduce',
		'@stdlib/utils/async/reduce-right'
	]
});

ns.push({
	'alias': 'reduceRightAsync',
	'path': '@stdlib/utils/async/reduce-right',
	'value': require( '@stdlib/utils/async/reduce-right' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/async/for-each-right',
		'@stdlib/utils/async/map-right',
		'@stdlib/utils/async/reduce',
		'@stdlib/utils/reduce-right'
	]
});

ns.push({
	'alias': 'reEOL',
	'path': '@stdlib/regexp/eol',
	'value': require( '@stdlib/regexp/eol' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'reExtendedLengthPath',
	'path': '@stdlib/regexp/extended-length-path',
	'value': require( '@stdlib/regexp/extended-length-path' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'reExtname',
	'path': '@stdlib/regexp/extname',
	'value': require( '@stdlib/regexp/extname' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/extname-posix',
		'@stdlib/regexp/extname-windows',
		'@stdlib/utils/extname'
	]
});

ns.push({
	'alias': 'reExtnamePosix',
	'path': '@stdlib/regexp/extname-posix',
	'value': require( '@stdlib/regexp/extname-posix' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/extname',
		'@stdlib/regexp/extname-windows',
		'@stdlib/utils/extname'
	]
});

ns.push({
	'alias': 'reExtnameWindows',
	'path': '@stdlib/regexp/extname-windows',
	'value': require( '@stdlib/regexp/extname-windows' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/extname',
		'@stdlib/regexp/extname-posix',
		'@stdlib/utils/extname'
	]
});

ns.push({
	'alias': 'reFilename',
	'path': '@stdlib/regexp/filename',
	'value': require( '@stdlib/regexp/filename' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/filename-posix',
		'@stdlib/regexp/filename-windows'
	]
});

ns.push({
	'alias': 'reFilenamePosix',
	'path': '@stdlib/regexp/filename-posix',
	'value': require( '@stdlib/regexp/filename-posix' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/filename',
		'@stdlib/regexp/filename-windows'
	]
});

ns.push({
	'alias': 'reFilenameWindows',
	'path': '@stdlib/regexp/filename-windows',
	'value': require( '@stdlib/regexp/filename-windows' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/filename',
		'@stdlib/regexp/filename-posix'
	]
});

ns.push({
	'alias': 'reFromString',
	'path': '@stdlib/utils/regexp-from-string',
	'value': require( '@stdlib/utils/regexp-from-string' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'reFunctionName',
	'path': '@stdlib/regexp/function-name',
	'value': require( '@stdlib/regexp/function-name' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/function-name'
	]
});

ns.push({
	'alias': 'reim',
	'path': '@stdlib/complex/reim',
	'value': require( '@stdlib/complex/reim' ),
	'type': 'Function',
	'related': [
		'@stdlib/complex/imag',
		'@stdlib/complex/real'
	]
});

ns.push({
	'alias': 'removeFirst',
	'path': '@stdlib/string/remove-first',
	'value': require( '@stdlib/string/remove-first' ),
	'type': 'Function',
	'related': [
		'@stdlib/string/remove-last'
	]
});

ns.push({
	'alias': 'removeLast',
	'path': '@stdlib/string/remove-last',
	'value': require( '@stdlib/string/remove-last' ),
	'type': 'Function',
	'related': [
		'@stdlib/string/remove-first'
	]
});

ns.push({
	'alias': 'removePunctuation',
	'path': '@stdlib/string/remove-punctuation',
	'value': require( '@stdlib/string/remove-punctuation' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'removeUTF8BOM',
	'path': '@stdlib/string/remove-utf8-bom',
	'value': require( '@stdlib/string/remove-utf8-bom' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'removeWords',
	'path': '@stdlib/string/remove-words',
	'value': require( '@stdlib/string/remove-words' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'rename',
	'path': '@stdlib/fs/rename',
	'value': require( '@stdlib/fs/rename' ),
	'type': 'Function',
	'related': [
		'@stdlib/fs/exists',
		'@stdlib/fs/read-file',
		'@stdlib/fs/write-file',
		'@stdlib/fs/unlink'
	]
});

ns.push({
	'alias': 'reNativeFunction',
	'path': '@stdlib/regexp/native-function',
	'value': require( '@stdlib/regexp/native-function' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/function-name',
		'@stdlib/utils/function-name'
	]
});

ns.push({
	'alias': 'reorderArguments',
	'path': '@stdlib/utils/reorder-arguments',
	'value': require( '@stdlib/utils/reorder-arguments' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/reverse-arguments'
	]
});

ns.push({
	'alias': 'repeat',
	'path': '@stdlib/string/repeat',
	'value': require( '@stdlib/string/repeat' ),
	'type': 'Function',
	'related': [
		'@stdlib/string/pad'
	]
});

ns.push({
	'alias': 'replace',
	'path': '@stdlib/string/replace',
	'value': require( '@stdlib/string/replace' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'reRegExp',
	'path': '@stdlib/regexp/regexp',
	'value': require( '@stdlib/regexp/regexp' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/regexp-from-string'
	]
});

ns.push({
	'alias': 'rescape',
	'path': '@stdlib/utils/escape-regexp-string',
	'value': require( '@stdlib/utils/escape-regexp-string' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'resolveParentPath',
	'path': '@stdlib/fs/resolve-parent-path',
	'value': require( '@stdlib/fs/resolve-parent-path' ),
	'type': 'Function',
	'related': [
		'@stdlib/fs/resolve-parent-path-by'
	]
});

ns.push({
	'alias': 'resolveParentPathBy',
	'path': '@stdlib/fs/resolve-parent-path-by',
	'value': require( '@stdlib/fs/resolve-parent-path-by' ),
	'type': 'Function',
	'related': [
		'@stdlib/fs/resolve-parent-path'
	]
});

ns.push({
	'alias': 'reUncPath',
	'path': '@stdlib/regexp/unc-path',
	'value': require( '@stdlib/regexp/unc-path' ),
	'type': 'Function',
	'related': [
		'@stdlib/assert/is-unc-path'
	]
});

ns.push({
	'alias': 'reUtf16SurrogatePair',
	'path': '@stdlib/regexp/utf16-surrogate-pair',
	'value': require( '@stdlib/regexp/utf16-surrogate-pair' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/utf16-unpaired-surrogate'
	]
});

ns.push({
	'alias': 'reUtf16UnpairedSurrogate',
	'path': '@stdlib/regexp/utf16-unpaired-surrogate',
	'value': require( '@stdlib/regexp/utf16-unpaired-surrogate' ),
	'type': 'Function',
	'related': [
		'@stdlib/regexp/utf16-surrogate-pair'
	]
});

ns.push({
	'alias': 'reverseArguments',
	'path': '@stdlib/utils/reverse-arguments',
	'value': require( '@stdlib/utils/reverse-arguments' ),
	'type': 'Function',
	'related': [
		'@stdlib/utils/reorder-arguments'
	]
});

ns.push({
	'alias': 'reverseString',
	'path': '@stdlib/string/reverse',
	'value': require( '@stdlib/string/reverse' ),
	'type': 'Function',
	'related': []
});

ns.push({
	'alias': 'reviveBasePRNG',
	'path': '@stdlib/random/base/reviver',
	'value': require( '@stdlib/random/base/reviver' ),
	'type': 'Function',
	'related': [
		'@stdlib/random/reviver'
	]
});

ns.push({
	'alias': 'reviveBuffer',
	'path': '@stdlib/buffer/reviver',
	'value': require( '@stdlib/buffer/reviver' ),
	'type': 'Function',
	'related': [
		'@stdlib/buffer/to-json'
	]
});

ns.push({
	'alias': 'reviveComplex',
	'path': '@stdlib/complex/reviver',
	'value': require( '@stdlib/complex/reviver' ),
	'type': 'Function',
	'related': [
		'@stdlib/complex/float64',
		'@stdlib/complex/float32',
		'@stdlib/complex/reviver-float64',
		'@stdlib/complex/reviver-float32'
	]
});

ns.push({
	'alias': 'reviveComplex64',
	'path': '@stdlib/complex/reviver-float32',
	'value': require( '@stdlib/complex/reviver-float32' ),
	'type': 'Function',
	'related': [
		'@stdlib/complex/float32',
		'@stdlib/complex/reviver-float64',
		'@stdlib/complex/reviver'
	]
});

ns.push({
	'alias': 'reviveComplex128',
	'path': '@stdlib/complex/reviver-float64',
	'value': require( '@stdlib/complex/reviver-float64' ),
	'type': 'Function',
	'related': [
		'@stdlib/complex/float64',
		'@stdlib/complex/reviver-float32',
		'@stdlib/complex/reviver'
	]
});

ns.push({
	'alias': 'reviveError',
	'path': '@stdlib/error/reviver',
	'value': require( '@stdlib/error/reviver' ),
	'type': 'Function',
	'related': [
		'@stdlib/error/to-json'
	]
});

ns.push({
	'alias': 'reviveTypedArray',
	'path': '@stdlib/array/reviver',
	'value': require( '@stdlib/array/reviver' ),
	'type': 'Function',
	'related': [
		'@stdlib/array/to-json'
	]
});

ns.push({
	'alias': 'reWhitespace',
	'path': '@stdlib/regexp/whitespace',
	'value': require( '@stdlib/regexp/whitespace' ),
	'type': 'Function',
	'related': [
		'@stdlib/assert/is-whitespace'
	]
});

ns.push({
	'alias': 'rpad',
	'path': '@stdlib/string/right-pad',
	'value': require( '@stdlib/string/right-pad' ),
	'type': 'Function',
	'related': [
		'@stdlib/string/left-pad',
		'@stdlib/string/pad'
	]
});

ns.push({
	'alias': 'rtrim',
	'path': '@stdlib/string/right-trim',
	'value': require( '@stdlib/string/right-trim' ),
	'type': 'Function',
	'related': [
		'@stdlib/string/left-trim',
		'@stdlib/string/trim'
	]
});


// EXPORTS //

module.exports = ns;

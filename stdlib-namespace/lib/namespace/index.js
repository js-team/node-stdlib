/**
* @license Apache-2.0
*
* Copyright (c) 2018 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

'use strict';

// MAIN //

/**
* Namespace.
*
* @private
* @namespace ns
*/
var ns = [];
ns = ns.concat( require( './a.js' ) );
ns = ns.concat( require( './b.js' ) );
ns = ns.concat( require( './c.js' ) );
ns = ns.concat( require( './d.js' ) );
ns = ns.concat( require( './e.js' ) );
ns = ns.concat( require( './f.js' ) );
ns = ns.concat( require( './g.js' ) );
ns = ns.concat( require( './h.js' ) );
ns = ns.concat( require( './i.js' ) );
ns = ns.concat( require( './j.js' ) );
ns = ns.concat( require( './k.js' ) );
ns = ns.concat( require( './l.js' ) );
ns = ns.concat( require( './m.js' ) );
ns = ns.concat( require( './n.js' ) );
ns = ns.concat( require( './o.js' ) );
ns = ns.concat( require( './p.js' ) );
ns = ns.concat( require( './q.js' ) );
ns = ns.concat( require( './r.js' ) );
ns = ns.concat( require( './s.js' ) );
ns = ns.concat( require( './t.js' ) );
ns = ns.concat( require( './u.js' ) );
ns = ns.concat( require( './v.js' ) );
ns = ns.concat( require( './w.js' ) );
ns = ns.concat( require( './x.js' ) );
ns = ns.concat( require( './y.js' ) );
ns = ns.concat( require( './z.js' ) );


// EXPORTS //

module.exports = ns;

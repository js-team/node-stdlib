
{{alias}}( [out,] re1, im1, re2, im2 )
    Divides two complex numbers.

    Parameters
    ----------
    out: Array|TypedArray|Object (optional)
        Output array.

    re1: number
        Real component.

    im1: number
        Imaginary component.

    re2: number
        Real component.

    im2: number
        Imaginary component.

    Returns
    -------
    out: Array|TypedArray|Object
        Real and imaginary components.

    Examples
    --------
    > var y = {{alias}}( -13.0, -1.0, -2.0, 1.0 )
    [ 5.0, 3.0 ]

    > var out = new {{alias:@stdlib/array/float64}}( 2 );
    > var v = {{alias}}( out, -13.0, -1.0, -2.0, 1.0 )
    <Float64Array>[ 5.0, 3.0 ]
    > var bool = ( v === out )
    true

    See Also
    --------


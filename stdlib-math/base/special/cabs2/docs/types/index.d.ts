/*
* @license Apache-2.0
*
* Copyright (c) 2019 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

// TypeScript Version: 2.0

/**
* Computes the squared absolute value of a complex number.
*
* ## Notes
*
* -   The absolute value of a complex number is its distance from zero.
*
* @param re - real component
* @param im - imaginary component
* @returns squared absolute value
*
* @example
* var v = cabs2( 5.0, 3.0 );
* // returns 34.0
*/
declare function cabs2( re: number, im: number ): number;


// EXPORTS //

export = cabs2;

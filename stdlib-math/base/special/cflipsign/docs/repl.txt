
{{alias}}( [out,] re, im, y )
    Returns a complex number with the same magnitude as `z` and the sign of
    `y*z`.

    Parameters
    ----------
    out: Array|TypedArray|Object (optional)
        Output array.

    re: number
        Real component.

    im: number
        Imaginary component.

    y: number
        Number from which to derive the sign.

    Returns
    -------
    out: Array|TypedArray|Object
        Function result.

    Examples
    --------
    > var out = {{alias}}( -4.2, 5.5, -9 )
    [ 4.2, -5.5 ]

    // Provide an output array:
    > out = new {{alias:@stdlib/array/float64}}( 2 );
    > var v = {{alias}}( out, -4.2, 5.5, 8 )
    <Float64Array>[ -4.2, 5.5 ]
    > var bool = ( v === out )
    true

    See Also
    --------


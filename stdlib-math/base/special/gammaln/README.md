<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# gammaln

> [Natural logarithm][@stdlib/math/base/special/ln] of the [gamma][@stdlib/math/base/special/gamma] function.

<section class="usage">

## Usage

```javascript
var gammaln = require( '@stdlib/math/base/special/gammaln' );
```

#### gammaln( x )

Evaluates the [natural logarithm][@stdlib/math/base/special/ln] of the [gamma function][@stdlib/math/base/special/gamma].

```javascript
var v = gammaln( 2.0 );
// returns 0.0

v = gammaln( 1.0 );
// returns 0.0

v = gammaln( 4.0 );
// returns ~1.792

v = gammaln( -0.5 );
// returns ~1.266

v = gammaln( 0.5 );
// returns ~0.572

v = gammaln( 0.0 );
// returns Infinity

v = gammaln( NaN );
// returns NaN
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var linspace = require( '@stdlib/array/linspace' );
var gammaln = require( '@stdlib/math/base/special/gammaln' );

var x = linspace( -10.0, 10.0, 100 );
var v;
var i;

for ( i = 0; i < x.length; i++ ) {
    v = gammaln( x[ i ] );
    console.log( 'x: %d, f(x): %d', x[ i ], v );
}
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/math/base/special/gamma]: https://github.com/stdlib-js/math/tree/main/base/special/gamma

[@stdlib/math/base/special/ln]: https://github.com/stdlib-js/math/tree/main/base/special/ln

</section>

<!-- /.links -->

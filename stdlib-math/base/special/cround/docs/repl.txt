
{{alias}}( [out,] re, im )
    Rounds a complex number to the nearest integer.

    Parameters
    ----------
    out: Array|TypedArray|Object (optional)
        Output array.

    re: number
        Real component.

    im: number
        Imaginary component.

    Returns
    -------
    out: Array|TypedArray|Object
        Rounded components.

    Examples
    --------
    > var out = {{alias}}( 5.5, 3.3 )
    [ 6.0, 3.0 ]

    // Provide an output array:
    > out = new {{alias:@stdlib/array/float64}}( 2 );
    > var v = {{alias}}( out, 5.5, 3.3 )
    <Float64Array>[ 6.0, 3.0 ]
    > var bool = ( v === out )
    true

    See Also
    --------


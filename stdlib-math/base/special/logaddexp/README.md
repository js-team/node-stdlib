<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# logaddexp

> Evaluates the [natural logarithm][@stdlib/math/base/special/ln] of `exp(x) + exp(y)`.

<section class="intro">

Log-domain computations are commonly used to increase accuracy and avoid underflow and overflow when very small or very large numbers are represented directly as limited-precision, floating-point numbers. For example, in statistics, evaluating `logaddexp()` is useful when probabilities are so small as to exceed the normal range of floating-point numbers.

</section>

<section class="usage">

## Usage

```javascript
var logaddexp = require( '@stdlib/math/base/special/logaddexp' );
```

#### logaddexp( x, y )

Evaluates the [natural logarithm][@stdlib/math/base/special/ln] of `exp(x) + exp(y)`.

```javascript
var v = logaddexp( 90.0, 90.0 );
// returns ~90.6931

v = logaddexp( -20.0, 90.0 );
// returns 90.0

v = logaddexp( 0.0, -100.0 );
// returns ~3.7201e-44

v = logaddexp( NaN, 1.0 );
// returns NaN
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var incrspace = require( '@stdlib/array/incrspace' );
var logaddexp = require( '@stdlib/math/base/special/logaddexp' );

var x;
var v;
var i;
var j;

x = incrspace( -100.0, 100.0, 1.0 );
for ( i = 0; i < x.length; i++ ) {
    for ( j = i; j < x.length; j++ ) {
        v = logaddexp( x[ i ], x[ j ] );
        console.log( 'x: %d, y: %d, f(x, y): %d', x[ i ], x[ j ], v );
    }
}
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/math/base/special/ln]: https://github.com/stdlib-js/math/tree/main/base/special/ln

</section>

<!-- /.links -->

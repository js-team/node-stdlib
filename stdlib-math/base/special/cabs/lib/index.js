/**
* @license Apache-2.0
*
* Copyright (c) 2018 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

'use strict';

/**
* Compute an absolute value of a complex number.
*
* @module @stdlib/math/base/special/cabs
*
* @example
* var cabs = require( '@stdlib/math/base/special/cabs' );
*
* var v = cabs( 5.0, 3.0 );
* // returns ~5.83
*/

// MODULES //

var cabs = require( './cabs.js' );


// EXPORTS //

module.exports = cabs;

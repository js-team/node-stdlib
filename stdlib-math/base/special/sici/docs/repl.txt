
{{alias}}( [out,] x )
    Computes the sine and cosine integrals.

    Parameters
    ----------
    out: Array|TypedArray|Object (optional)
        Output array.

    x: number
        Input value.

    Returns
    -------
    out: Array|TypedArray|Object
        Sine and cosine integrals.

    Examples
    --------
    > var y = {{alias}}( 3.0 )
    [ ~1.849, ~0.12 ]
    > y = {{alias}}( 0.0 )
    [ 0.0, -Infinity ]
    > y = {{alias}}( -9.0 )
    [ ~-1.665, ~0.055 ]
    > y = {{alias}}( NaN )
    [ NaN, NaN ]

    // Provide an output array:
    > var out = new {{alias:@stdlib/array/float64}}( 2 );
    > y = {{alias}}( out, 3.0 )
    <Float64Array>[ ~1.849, ~0.12 ]
    > var bool = ( y === out )
    true

    See Also
    --------


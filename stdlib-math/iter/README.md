<!--

@license Apache-2.0

Copyright (c) 2020 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Math Iterators

> Standard library math iterators.

<section class="usage">

## Usage

```javascript
var ns = require( '@stdlib/math/iter' );
```

#### ns

Standard library math iterators.

```javascript
var iterators = ns;
// returns {...}
```

The namespace contains the following sub-namespaces:

<!-- <toc pattern="*"> -->

<div class="namespace-toc">

-   <span class="signature">[`ops`][@stdlib/math/iter/ops]</span><span class="delimiter">: </span><span class="description">standard library math operator iterators.</span>
-   <span class="signature">[`sequences`][@stdlib/math/iter/sequences]</span><span class="delimiter">: </span><span class="description">standard library math iterators for generating sequences.</span>
-   <span class="signature">[`special`][@stdlib/math/iter/special]</span><span class="delimiter">: </span><span class="description">standard library math iterators for special functions.</span>

</div>

<!-- </toc> -->

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better examples -->

<!-- eslint no-undef: "error" -->

```javascript
var objectKeys = require( '@stdlib/utils/keys' );
var ns = require( '@stdlib/math/iter' );

console.log( objectKeys( ns ) );
```

</section>

<!-- /.examples -->

<section class="links">

<!-- <toc-links> -->

[@stdlib/math/iter/ops]: https://github.com/stdlib-js/math/tree/main/iter/ops

[@stdlib/math/iter/sequences]: https://github.com/stdlib-js/math/tree/main/iter/sequences

[@stdlib/math/iter/special]: https://github.com/stdlib-js/math/tree/main/iter/special

<!-- </toc-links> -->

</section>

<!-- /.links -->

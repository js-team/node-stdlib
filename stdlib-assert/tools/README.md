<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# Assert

> Standard library assertion utility tools.

<section class="usage">

## Usage

```javascript
var tools = require( '@stdlib/assert/tools' );
```

#### tools

Standard library assertion utility tools.

```javascript
var o = tools;
// returns {...}
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- TODO: better examples -->

<!-- eslint no-undef: "error" -->

```javascript
var objectKeys = require( '@stdlib/utils/keys' );
var tools = require( '@stdlib/assert/tools' );

console.log( objectKeys( tools ) );
```

</section>

<!-- /.examples -->

<section class="links">

</section>

<!-- /.links -->

<!--

@license Apache-2.0

Copyright (c) 2018 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# isndarrayLike

> Test if a value is [ndarray][@stdlib/ndarray/ctor]-like.

<section class="usage">

## Usage

```javascript
var isndarrayLike = require( '@stdlib/assert/is-ndarray-like' );
```

#### isndarrayLike( value )

Tests if a value is [ndarray][@stdlib/ndarray/ctor]-like.

```javascript
var ndarray = require( '@stdlib/ndarray/ctor' );

var arr = ndarray( 'generic', [ 0, 0, 0, 0 ], [ 2, 2 ], [ 2, 1 ], 0, 'row-major' );
var bool = isndarrayLike( arr );
// returns true
```

A value is [ndarray][@stdlib/ndarray/ctor]-like if a value is an `object` with the following properties:

-   **dtype**: `string` specifying a data type.
-   **data**: `object` pointing to an underlying data buffer.
-   **shape**: array-like `object` containing dimensions.
-   **strides**: array-like `object` containing stride lengths.
-   **offset**: `number` specifying the index offset.
-   **order**: `string` describing the memory layout.
-   **ndims**: `number` specifying the number of dimensions.
-   **length**: `number` specifying the total number of elements.
-   **flags**: `object` containing meta data.
-   **get**: `function` for retrieving elements.
-   **set**: `function` for setting elements.

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var ndarray = require( '@stdlib/ndarray/ctor' );
var isndarrayLike = require( '@stdlib/assert/is-ndarray-like' );

var arr = ndarray( 'generic', [ 0, 0, 0, 0 ], [ 2, 2 ], [ 2, 1 ], 0, 'row-major' );
var bool = isndarrayLike( arr );
// returns true

bool = isndarrayLike( [ 1, 2, 3, 4 ] );
// returns false

bool = isndarrayLike( {} );
// returns false

bool = isndarrayLike( null );
// returns false
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/ndarray/ctor]: https://github.com/stdlib-js/ndarray-ctor

</section>

<!-- /.links -->

<!--

@license Apache-2.0

Copyright (c) 2020 The Stdlib Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

-->

# isFloat32ndarrayLike

> Test if a value is an [ndarray][@stdlib/ndarray/ctor]-like object containing single-precision floating-point numbers.

<section class="usage">

## Usage

```javascript
var isFloat32ndarrayLike = require( '@stdlib/assert/is-float32ndarray-like' );
```

#### isFloat32ndarrayLike( value )

Tests if a value is an [ndarray][@stdlib/ndarray/ctor]-like object whose underlying data type is `float32`.

```javascript
var Float32Array = require( '@stdlib/array/float32' );
var ndarray = require( '@stdlib/ndarray/ctor' );

var arr = ndarray( 'float32', new Float32Array( [ 0, 0, 0, 0 ] ), [ 2, 2 ], [ 2, 1 ], 0, 'row-major' );

var bool = isFloat32ndarrayLike( arr );
// returns true
```

</section>

<!-- /.usage -->

<section class="examples">

## Examples

<!-- eslint no-undef: "error" -->

```javascript
var ndarray = require( '@stdlib/ndarray/ctor' );
var Float32Array = require( '@stdlib/array/float32' );
var isFloat32ndarrayLike = require( '@stdlib/assert/is-float32ndarray-like' );

var buffer = new Float32Array( [ 0, 0, 0, 0 ] );
var arr = ndarray( 'float32', buffer, [ 2, 2 ], [ 2, 1 ], 0, 'row-major' );

var out = isFloat32ndarrayLike( arr );
// returns true

out = isFloat32ndarrayLike( [ 1, 2, 3, 4 ] );
// returns false

out = isFloat32ndarrayLike( {} );
// returns false

out = isFloat32ndarrayLike( null );
// returns false
```

</section>

<!-- /.examples -->

<section class="links">

[@stdlib/ndarray/ctor]: https://github.com/stdlib-js/ndarray-ctor

</section>

<!-- /.links -->

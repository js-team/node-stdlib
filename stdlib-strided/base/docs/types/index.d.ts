/*
* @license Apache-2.0
*
* Copyright (c) 2021 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

// TypeScript Version: 2.0

/* tslint:disable:max-line-length */
/* tslint:disable:max-file-line-count */

import binary = require( './../../../base/binary' );
import dmap = require( './../../../base/dmap' );
import dmskmap = require( './../../../base/dmskmap' );
import mapBy = require( './../../../base/map-by' );
import mskunary = require( './../../../base/mskunary' );
import nullary = require( './../../../base/nullary' );
import quaternary = require( './../../../base/quaternary' );
import quinary = require( './../../../base/quinary' );
import smap = require( './../../../base/smap' );
import smskmap = require( './../../../base/smskmap' );
import ternary = require( './../../../base/ternary' );
import unary = require( './../../../base/unary' );

/**
* Interface describing the `base` namespace.
*/
interface Namespace {
	/**
	* Applies a binary callback to strided input array elements and assigns results to elements in a strided output array.
	*
	* @param arrays - array-like object containing two input arrays and one output array
	* @param shape - array-like object containing a single element, the number of indexed elements
	* @param strides - array-like object containing the stride lengths for the input and output arrays
	* @param fcn - binary callback
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y ) {
	*     return x + y;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1 ];
	*
	* ns.binary( [ x, y, z ], shape, strides, add );
	*
	* console.log( z );
	* // => <Float64Array>[ 2.0, 4.0, 6.0, 8.0, 10.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y ) {
	*     return x + y;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1 ];
	* var offsets = [ 0, 0, 0 ];
	*
	* ns.binary.ndarray( [ x, y, z ], shape, strides, offsets, add );
	*
	* console.log( z );
	* // => <Float64Array>[ 2.0, 4.0, 6.0, 8.0, 10.0 ]
	*/
	binary: typeof binary;

	/**
	* Applies a unary function accepting and returning double-precision floating-point numbers to each element in a double-precision floating-point strided input array and assigns each result to an element in a double-precision floating-point strided output array.
	*
	* @param N - number of indexed elements
	* @param x - input array
	* @param strideX - `x` stride length
	* @param y - destination array
	* @param strideY - `y` stride length
	* @param fcn - unary function to apply
	* @returns `y`
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.dmap( x.length, x, 1, y, 1, identity );
	* // y => <Float64Array>[ 1.0, 2.0, 3.0, 4.0, 5.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.dmap.ndarray( x.length, x, 1, 0, y, 1, 0, identity );
	* // y => <Float64Array>[ 1.0, 2.0, 3.0, 4.0, 5.0 ]
	*/
	dmap: typeof dmap;

	/**
	* Applies a unary function accepting and returning double-precision floating-point numbers to each element in a double-precision floating-point strided input array according to a corresponding element in a strided mask array and assigns each result to an element in a double-precision floating-point strided output array.
	*
	* @param N - number of indexed elements
	* @param x - input array
	* @param strideX - `x` stride length
	* @param mask - mask array
	* @param strideMask - `mask` stride length
	* @param y - destination array
	* @param strideY - `y` stride length
	* @param fcn - unary function to apply
	* @returns `y`
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	* var Uint8Array = require( `@stdlib/array/uint8` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var m = new Uint8Array( [ 0, 0, 1, 0, 0 ] );
	* var y = new Float64Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.dmskmap( x.length, x, 1, m, 1, y, 1, identity );
	* // y => <Float64Array>[ 1.0, 2.0, 0.0, 4.0, 5.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	* var Uint8Array = require( `@stdlib/array/uint8` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var m = new Uint8Array( [ 0, 0, 1, 0, 0 ] );
	* var y = new Float64Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.dmskmap.ndarray( x.length, x, 1, 0, m, 1, 0, y, 1, 0, identity );
	* // y => <Float64Array>[ 1.0, 2.0, 0.0, 4.0, 5.0 ]
	*/
	dmskmap: typeof dmskmap;

	/**
	* Applies a unary function to each element retrieved from a strided input array according to a callback function and assigns each result to an element in a strided output array.
	*
	* @param N - number of indexed elements
	* @param x - input array
	* @param strideX - `x` stride length
	* @param y - destination array
	* @param strideY - `y` stride length
	* @param fcn - unary function to apply to callback return values
	* @param clbk - callback function
	* @param thisArg - callback execution context
	* @returns `y`
	*
	* @example
	* var abs = require( `@stdlib/math/base/special/abs` );
	*
	* function accessor( v ) {
	*     return v * 2.0;
	* }
	*
	* var x = [ 1.0, -2.0, 3.0, -4.0, 5.0 ];
	* var y = [ 0.0, 0.0, 0.0, 0.0, 0.0 ];
	*
	* ns.mapBy( x.length, x, 1, y, 1, abs, accessor );
	* // y => [ 2.0, 4.0, 6.0, 8.0, 10.0 ]
	*
	* @example
	* var abs = require( `@stdlib/math/base/special/abs` );
	*
	* function accessor( v ) {
	*     return v * 2.0;
	* }
	*
	* var x = [ 1.0, -2.0, 3.0, -4.0, 5.0 ];
	* var y = [ 0.0, 0.0, 0.0, 0.0, 0.0 ];
	*
	* ns.mapBy.ndarray( x.length, x, 1, 0, y, 1, 0, abs, accessor );
	* // y => [ 2.0, 4.0, 6.0, 8.0, 10.0 ]
	*/
	mapBy: typeof mapBy;

	/**
	* Applies a unary callback to elements in a strided input array according to elements in a strided mask array and assigns results to elements in a strided output array.
	*
	* @param arrays - array-like object containing one input array, a mask array, and one output array
	* @param shape - array-like object containing a single element, the number of indexed elements
	* @param strides - array-like object containing the stride lengths for the strided arrays
	* @param fcn - unary callback
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	* var Uint8Array = require( `@stdlib/array/uint8` );
	*
	* function scale( x ) {
	*     return x * 10.0;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var m = new Uint8Array( [ 0, 0, 1, 0, 0 ] );
	* var y = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1 ];
	*
	* ns.mskunary( [ x, m, y ], shape, strides, scale );
	*
	* console.log( y );
	* // => <Float64Array>[ 10.0, 20.0, 0.0, 40.0, 50.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	* var Uint8Array = require( `@stdlib/array/uint8` );
	*
	* function scale( x ) {
	*     return x * 10.0;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var m = new Uint8Array( [ 0, 0, 1, 0, 0 ] );
	* var y = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1 ];
	* var offsets = [ 0, 0, 0 ];
	*
	* ns.mskunary.ndarray( [ x, m, y ], shape, strides, offsets, scale );
	*
	* console.log( y );
	* // => <Float64Array>[ 10.0, 20.0, 0.0, 40.0, 50.0 ]
	*/
	mskunary: typeof mskunary;

	/**
	* Applies a nullary callback and assigns results to elements in a strided output array.
	*
	* @param arrays - array-like object containing one output array
	* @param shape - array-like object containing a single element, the number of indexed elements
	* @param strides - array-like object containing the stride length for the output array
	* @param fcn - nullary callback
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function fill() {
	*     return 3.0;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	*
	* var shape = [ x.length ];
	* var strides = [ 1 ];
	*
	* ns.nullary( [ x ], shape, strides, fill );
	*
	* console.log( x );
	* // => <Float64Array>[ 3.0, 3.0, 3.0, 3.0, 3.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function fill() {
	*     return 3.0;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	*
	* var shape = [ x.length ];
	* var strides = [ 1 ];
	* var offsets = [ 0 ];
	*
	* ns.nullary.ndarray( [ x ], shape, strides, offsets, fill );
	*
	* console.log( x );
	* // => <Float64Array>[ 3.0, 3.0, 3.0, 3.0, 3.0 ]
	*/
	nullary: typeof nullary;

	/**
	* Applies a quaternary callback to strided input array elements and assigns results to elements in a strided output array.
	*
	* @param arrays - array-like object containing four input arrays and one output array
	* @param shape - array-like object containing a single element, the number of indexed elements
	* @param strides - array-like object containing the stride lengths for the input and output arrays
	* @param fcn - quaternary callback
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y, z, w ) {
	*     return x + y + z + w;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var w = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var u = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1, 1, 1 ];
	*
	* ns.quaternary( [ x, y, z, w, u ], shape, strides, add );
	*
	* console.log( u );
	* // => <Float64Array>[ 4.0, 8.0, 12.0, 16.0, 20.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y, z, w ) {
	*     return x + y + z + w;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var w = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var u = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1, 1, 1 ];
	* var offsets = [ 0, 0, 0, 0, 0 ];
	*
	* ns.quaternary.ndarray( [ x, y, z, w, u ], shape, strides, offsets, add );
	*
	* console.log( u );
	* // => <Float64Array>[ 4.0, 8.0, 12.0, 16.0, 20.0 ]
	*/
	quaternary: typeof quaternary;

	/**
	* Applies a quinary callback to strided input array elements and assigns results to elements in a strided output array.
	*
	* @param arrays - array-like object containing five input arrays and one output array
	* @param shape - array-like object containing a single element, the number of indexed elements
	* @param strides - array-like object containing the stride lengths for the input and output arrays
	* @param fcn - quinary callback
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y, z, w, u ) {
	*     return x + y + z + w + u;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var w = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var u = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var v = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1, 1, 1, 1 ];
	*
	* ns.quinary( [ x, y, z, w, u, v ], shape, strides, add );
	*
	* console.log( v );
	* // => <Float64Array>[ 5.0, 10.0, 15.0, 20.0, 25.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y, z, w, u ) {
	*     return x + y + z + w + u;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var w = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var u = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var v = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1, 1, 1, 1 ];
	* var offsets = [ 0, 0, 0, 0, 0, 0 ];
	*
	* ns.quinary.ndarray( [ x, y, z, w, u, v ], shape, strides, offsets, add );
	*
	* console.log( v );
	* // => <Float64Array>[ 5.0, 10.0, 15.0, 20.0, 25.0 ]
	*/
	quinary: typeof quinary;

	/**
	* Applies a unary function accepting and returning single-precision floating-point numbers to each element in a single-precision floating-point strided input array and assigns each result to an element in a single-precision floating-point strided output array.
	*
	* @param N - number of indexed elements
	* @param x - input array
	* @param strideX - `x` stride length
	* @param y - destination array
	* @param strideY - `y` stride length
	* @param fcn - unary function to apply
	* @returns `y`
	*
	* @example
	* var Float32Array = require( `@stdlib/array/float32` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float32Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float32Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.smap( x.length, x, 1, y, 1, identity );
	* // y => <Float32Array>[ 1.0, 2.0, 3.0, 4.0, 5.0 ]
	*
	* @example
	* var Float32Array = require( `@stdlib/array/float32` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float32Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float32Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.smap.ndarray( x.length, x, 1, 0, y, 1, 0, identity );
	* // y => <Float32Array>[ 1.0, 2.0, 3.0, 4.0, 5.0 ]
	*/
	smap: typeof smap;

	/**
	* Applies a unary function accepting and returning single-precision floating-point numbers to each element in a single-precision floating-point strided input array according to a corresponding element in a strided mask array and assigns each result to an element in a single-precision floating-point strided output array.
	*
	* @param N - number of indexed elements
	* @param x - input array
	* @param strideX - `x` stride length
	* @param mask - mask array
	* @param strideMask - `mask` stride length
	* @param y - destination array
	* @param strideY - `y` stride length
	* @param fcn - unary function to apply
	* @returns `y`
	*
	* @example
	* var Float32Array = require( `@stdlib/array/float32` );
	* var Uint8Array = require( `@stdlib/array/uint8` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float32Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var m = new Uint8Array( [ 0, 0, 1, 0, 0 ] );
	* var y = new Float32Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.smskmap( x.length, x, 1, m, 1, y, 1, identity );
	* // y => <Float32Array>[ 1.0, 2.0, 0.0, 4.0, 5.0 ]
	*
	* @example
	* var Float32Array = require( `@stdlib/array/float32` );
	* var Uint8Array = require( `@stdlib/array/uint8` );
	*
	* function identity( x ) {
	*     return x;
	* }
	*
	* var x = new Float32Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var m = new Uint8Array( [ 0, 0, 1, 0, 0 ] );
	* var y = new Float32Array( [ 0.0, 0.0, 0.0, 0.0, 0.0 ] );
	*
	* ns.smskmap.ndarray( x.length, x, 1, 0, m, 1, 0, y, 1, 0, identity );
	* // y => <Float32Array>[ 1.0, 2.0, 0.0, 4.0, 5.0 ]
	*/
	smskmap: typeof smskmap;

	/**
	* Applies a ternary callback to strided input array elements and assigns results to elements in a strided output array.
	*
	* @param arrays - array-like object containing three input arrays and one output array
	* @param shape - array-like object containing a single element, the number of indexed elements
	* @param strides - array-like object containing the stride lengths for the input and output arrays
	* @param fcn - ternary callback
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y, z ) {
	*     return x + y + z;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var w = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1, 1 ];
	*
	* ns.ternary( [ x, y, z, w ], shape, strides, add );
	*
	* console.log( w );
	* // => <Float64Array>[ 3.0, 6.0, 9.0, 12.0, 15.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function add( x, y, z ) {
	*     return x + y + z;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var z = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var w = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1, 1, 1 ];
	* var offsets = [ 0, 0, 0, 0 ];
	*
	* ns.ternary.ndarray( [ x, y, z, w ], shape, strides, offsets, add );
	*
	* console.log( w );
	* // => <Float64Array>[ 3.0, 6.0, 9.0, 12.0, 15.0 ]
	*/
	ternary: typeof ternary;

	/**
	* Applies a unary callback to elements in a strided input array and assigns results to elements in a strided output array.
	*
	* @param arrays - array-like object containing one input array and one output array
	* @param shape - array-like object containing a single element, the number of indexed elements
	* @param strides - array-like object containing the stride lengths for the input and output arrays
	* @param fcn - unary callback
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function scale( x ) {
	*     return x * 10.0;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1 ];
	*
	* ns.unary( [ x, y ], shape, strides, scale );
	*
	* console.log( y );
	* // => <Float64Array>[ 10.0, 20.0, 30.0, 40.0, 50.0 ]
	*
	* @example
	* var Float64Array = require( `@stdlib/array/float64` );
	*
	* function scale( x ) {
	*     return x * 10.0;
	* }
	*
	* var x = new Float64Array( [ 1.0, 2.0, 3.0, 4.0, 5.0 ] );
	* var y = new Float64Array( x.length );
	*
	* var shape = [ x.length ];
	* var strides = [ 1, 1 ];
	* var offsets = [ 0, 0 ];
	*
	* ns.unary.ndarray( [ x, y ], shape, strides, offsets, scale );
	*
	* console.log( y );
	* // => <Float64Array>[ 10.0, 20.0, 30.0, 40.0, 50.0 ]
	*/
	unary: typeof unary;
}

/**
* Base strided.
*/
declare var ns: Namespace;


// EXPORTS //

export = ns;

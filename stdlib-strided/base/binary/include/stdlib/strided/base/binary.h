/**
* @license Apache-2.0
*
* Copyright (c) 2018 The Stdlib Authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/**
* Header file containing function declarations for strided array functions which apply a binary callback.
*/
#ifndef STDLIB_STRIDED_BASE_BINARY_H
#define STDLIB_STRIDED_BASE_BINARY_H

#include "binary/macros.h"
#include "binary/typedefs.h"

// Note: keep in alphabetical order...
#include "binary/bb_b.h"
#include "binary/bb_d.h"
#include "binary/bb_d_as_dd_d.h"
#include "binary/bb_f.h"
#include "binary/bb_f_as_dd_d.h"
#include "binary/bb_f_as_ff_f.h"
#include "binary/bb_i.h"
#include "binary/bb_i_as_ii_i.h"
#include "binary/bb_k.h"
#include "binary/bb_k_as_kk_k.h"
#include "binary/bb_t.h"
#include "binary/bb_t_as_tt_t.h"
#include "binary/bb_u.h"
#include "binary/bb_u_as_uu_u.h"

#include "binary/dd_d.h"

#include "binary/ff_f_as_dd_d.h"
#include "binary/ff_f.h"

#include "binary/ii_d.h"
#include "binary/ii_d_as_dd_d.h"
#include "binary/ii_i.h"

#include "binary/kk_d.h"
#include "binary/kk_d_as_dd_d.h"
#include "binary/kk_f.h"
#include "binary/kk_f_as_dd_d.h"
#include "binary/kk_f_as_ff_f.h"
#include "binary/kk_i.h"
#include "binary/kk_i_as_ii_i.h"
#include "binary/kk_k.h"

#include "binary/ss_d.h"
#include "binary/ss_d_as_dd_d.h"
#include "binary/ss_f.h"
#include "binary/ss_f_as_dd_d.h"
#include "binary/ss_f_as_ff_f.h"
#include "binary/ss_i.h"
#include "binary/ss_i_as_ii_i.h"
#include "binary/ss_k.h"
#include "binary/ss_k_as_kk_k.h"
#include "binary/ss_s.h"

#include "binary/tt_d.h"
#include "binary/tt_d_as_dd_d.h"
#include "binary/tt_f.h"
#include "binary/tt_f_as_dd_d.h"
#include "binary/tt_f_as_ff_f.h"
#include "binary/tt_i.h"
#include "binary/tt_i_as_ii_i.h"
#include "binary/tt_t.h"
#include "binary/tt_u.h"
#include "binary/tt_u_as_uu_u.h"

#include "binary/uu_d.h"
#include "binary/uu_d_as_dd_d.h"
#include "binary/uu_u.h"

#endif // !STDLIB_STRIDED_BASE_BINARY_H
